var a, addFirstNode, addFirstNodeButtonClick, addFirstNodeCode, addLevelButton, addLevelButtonCode, addLink, addNodeButtonClick, addNodeButtonCode, addNodeButtons, array, backButtonClick, clickButtonActive, clickStart, combine, coolArray, createLevel, createLine, defaultBackground, delButtonClick, deleteId, deleteLevel, deleteLevelButton, deleteLink, deleteNode, deleteNodeButtons, doneButtonClick, dragHover, dragHoverLevel, dragLeave, dragLeaveLevel, dragStart, dragStartLevel, drop, dropLevel, editButtonClick, firstClickLink, fixLines, fixLines2, from, fromArray, fromL, getParentId, getParentParentId, getParentParentParentId, getRandomInt, getTime, lala, levelClass, levelCode, levelContainerClass, levelFrom, levelId, levelTo, linesDown, linesUp, linkButton, loadNodes, lowercaseFirstLetter, lowercaseFirstLetterAllWords, manageInput, merge, nodeClass, nodeCode, nodeFromNumber, nodeToNumber, outputNodeCode, parseId, removeLevelButtonClick, root, saveNodes, sortLevelId, sortNodeId, start, tabIndexOrder, textArray, thinLine, to, toArray, ua, uppercaseFirstLetter, uppercaseFirstLetterAllWords, version, widthSlider, writeError, writeId, writeMessage, wtf, x1, x2, y1, y2;

levelId = 1;

levelClass = "level";

levelContainerClass = "levelContanter";

nodeClass = "node";

array = [];

from = '';

fromL = '';

to = '';

levelFrom = '';

levelTo = '';

nodeFromNumber = '';

nodeToNumber = '';

x1 = 0;

y1 = 0;

x2 = 0;

y2 = 0;

thinLine = 0;

fromArray = [];

toArray = [];

a = 0;

coolArray = [[]];

wtf = false;

clickButtonActive = false;

linkButton = '';

firstClickLink = '';

widthSlider = [];

ua = navigator.userAgent;

textArray = [];

version = "v0.7.150330vaj";

root = typeof exports !== "undefined" && exports !== null ? exports : this;

root.editingNode = '';

outputNodeCode = "<div class='outputNode'> <div class='outputNodeName'></div> <textarea class='outputNodeContent'></textarea> </div>";

levelCode = "<div> <div class='levelButtonsWrapper'> <div class='removeLevelButtonWrapper'> <button class='removeLevelButton'>remove level</button> </div> <div class='levelText'> <input class='levelName' placeholder='level name'> </div> <div class='levelDragArea' ondrop='dropLevel(event)' ondragover='dragHoverLevel(event)' ondragleave='dragLeaveLevel(event)'></div> <div class='levelDragButton' draggable='true' ondragstart='dragStartLevel(event)'></div> </div> </div>";

nodeCode = "<div> <div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'> <textarea class='nodeText'></textarea> </div> <div class='buttons'> <div class='linkButton' onclick='clickStart(event)'></div> <div class='dndButton' draggable='true' ondragstart='dragStart(event)'></div> <div class='deleteLinkButton' onclick='clickStart(event)'></div> <div class='deleteDragButton' draggable='true' ondragstart='dragStart(event)'></div> <button class='editButton'>edit</button> <input class='nodeName' placeholder='node name'> <button class='delButton'>delete</button> </div> </div>";

addNodeButtonCode = "<div class='addNodeButtonWrapper'> <button class='addNodeButton'>add node</button> </div>";

addLevelButtonCode = "<div class='addLevelButtonWrapper'> <button class='addLevelButton'>add level</button> </div>";

addFirstNodeCode = "<div class='" + nodeClass + " nodeEmpty'> <div class='addNodeButtonWrapper'> <button class='addFirstNode'>add node</button> </div> </div>";

uppercaseFirstLetter = function(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

lowercaseFirstLetter = function(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
};

uppercaseFirstLetterAllWords = function(str) {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

lowercaseFirstLetterAllWords = function(str) {
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toLowerCase() + txt.substr(1).toUpperCase();
  });
};

getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

getTime = function(flag) {
  var hours, minutes, now, seconds, time;
  now = new Date();
  hours = now.getHours();
  minutes = now.getMinutes();
  seconds = now.getSeconds();
  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }
  if (flag) {
    return time = hours + ':' + minutes + ':' + seconds + ': ';
  } else {
    return now;
  }
};

Object.size = function(obj) {
  var key, size;
  size = 0;
  key = void 0;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      size++;
    }
  }
  return size;
};

getParentParentParentId = function(item) {
  return item.parent().parent().parent().attr('id');
};

getParentParentId = function(item) {
  return item.parent().parent().attr('id');
};

getParentId = function(item) {
  return item.parent().attr('id');
};

linesUp = function() {
  return $(".line").each(function(i) {
    var asd;
    asd = parseInt($(this).css("top"));
    asd--;
    asd = asd + "px";
    return $(this).css({
      "top": asd
    });
  });
};

linesDown = function() {
  return $(".line").each(function(i) {
    var asd;
    asd = parseInt($(this).css("top"));
    asd++;
    asd = asd + "px";
    return $(this).css({
      "top": asd
    });
  });
};

addFirstNode = function(level) {
  var nodeId;
  nodeId = level + "x1";
  return $("#" + levelClass + level).append($(nodeCode).attr("id", nodeClass + nodeId).attr("class", nodeClass));
};

createLevel = function() {
  $("." + levelContainerClass).append($(levelCode).attr("id", levelClass + levelId).attr("class", levelClass));
  return levelId++;
};

parseId = function(id) {
  var arrayR, foo;
  foo = id.substring(4);
  arrayR = foo.split("x");
  return arrayR;
};

sortNodeId = function(id) {
  var arrayR;
  arrayR = parseId(id);
  return $("#" + levelClass + arrayR[0] + " ." + nodeClass).each(function(i) {
    return $(this).attr('id', nodeClass + arrayR[0] + 'x' + (i + 1));
  });
};

sortLevelId = function() {
  return $("." + levelClass).each(function(i) {
    $(this).attr('id', levelClass + (i + 1));
    return $(this).children("." + nodeClass).each(function(j) {
      return $(this).attr('id', nodeClass + (i + 1) + 'x' + (j + 1));
    });
  });
};

addNodeButtons = function() {
  return $("." + levelClass + " ." + nodeClass + ":last-child").each(function(i) {
    if (!($(this).hasClass("nodeEmpty"))) {
      return $(this).append(addNodeButtonCode);
    }
  });
};

deleteNodeButtons = function() {
  return $(".addNodeButtonWrapper").each(function(i) {
    if (!($(this).parent().hasClass("nodeEmpty"))) {
      return $(this).remove();
    }
  });
};

addLevelButton = function() {
  return $("." + levelClass + ":last-child .levelButtonsWrapper").append(addLevelButtonCode);
};

deleteLevelButton = function() {
  return $(".addLevelButtonWrapper").remove();
};

deleteId = function() {
  return $(".matrixPosition").remove();
};

writeId = function(node) {
  deleteId();
  return $("." + nodeClass).each(function(i) {
    var id;
    id = $(this).attr('id').substring(4);
    return $(this).children(".buttons").append("<span class='matrixPosition'>" + id + "</span>");
  });
};

tabIndexOrder = function() {
  var i;
  i = 1;
  $(".levelName").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
  $(".nodeName").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
  return $(".nodeText").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
};

writeError = function(message) {
  return $(".console").prepend(getTime(true) + '<span style="color:red;">ERROR: </span>' + message + '<br>');
};

writeMessage = function(message) {
  return $(".console").prepend(getTime(true) + '<span style="color:#00AA00;">' + message + '</span><br>');
};

defaultBackground = function(ev) {
  return ev.target.style.background = "#EBEBE4";
};

deleteNode = function(node, a) {
  var arrayR, id, nextNode;
  if (a) {
    id = node;
  } else {
    id = getParentParentId(node);
  }
  $("#" + id).remove();
  arrayR = parseId(id);
  nextNode = parseInt(arrayR[1]);
  nextNode++;
  if (arrayR[1] === "1" && $('#' + nodeClass + arrayR[0] + 'x' + nextNode).length === 0) {
    return $("#" + levelClass + arrayR[0]).append(addFirstNodeCode);
  } else {
    sortNodeId(id);
    deleteNodeButtons();
    addNodeButtons();
    writeId();
    return tabIndexOrder();
  }
};

deleteLevel = function(level, a) {
  var bar, id;
  if (a) {
    id = level;
  } else {
    id = getParentParentParentId($(level));
  }
  bar = id.substring(5);
  if (bar === "1" && $('#' + levelClass + "2").length === 0) {
    return $(".lastLevelModal").modal();
  } else {
    $("#" + id).remove();
    levelId--;
    sortLevelId();
    deleteLevelButton();
    addLevelButton();
    writeId();
    return tabIndexOrder();
  }
};

saveNodes = function() {
  var baz, cookieString, levelNameArray, nodaNameArray;
  textArray = [];
  nodaNameArray = [];
  levelNameArray = [];
  baz = "";
  cookieString = "";
  $(".node").each(function() {
    textArray.push($(this).children(".nodeCanvas").children("textarea").val());
    return nodaNameArray.push($(this).children(".buttons").children(".nodeName").val());
  });
  $(".level").each(function() {
    $(this).children(".node").each(function() {
      var bar, foo;
      foo = $(this).attr("id");
      bar = parseId(foo);
      return baz = bar[1];
    });
    cookieString += baz + "y";
    return levelNameArray.push($(this).children(".levelButtonsWrapper").children(".levelText").children(".levelName").val());
  });
  textArray.reverse();
  nodaNameArray.reverse();
  levelNameArray.reverse();
  $.cookie("nodes", cookieString);
  $.cookie("nodesContents", JSON.stringify(textArray));
  $.cookie("nodesNames", JSON.stringify(nodaNameArray));
  return $.cookie("levelNames", JSON.stringify(levelNameArray));
};


/*
levelId = 1;
levelClass = "level"
levelContainerClass = "levelContanter"
nodeClass = "node"
array = []
from = ''
fromL = ''
to = ''
levelFrom = ''
levelTo = ''
nodeFromNumber = ''
nodeToNumber = ''
x1 = 0
y1 = 0
x2 = 0
y2 = 0
thinLine = 0
fromArray = []
toArray = []
a = 0
coolArray = [[]]
wtf = false
clickButtonActive = false
linkButton = ''
firstClickLink = ''
widthSlider = []
ua = navigator.userAgent
textArray = []

x5
 */

loadNodes = function() {
  var i, lev, levelNames2, nod, nodesNames2, nvar, q, temp, textArray2, ttt, w, ww, _i, _j, _ref;
  $(".level").remove();
  levelId = 1;
  nvar = $.cookie("nodes");
  lev = "";
  nod = "";
  temp = "";
  for (i = _i = 0, _ref = nvar.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
    if (nvar[i] === "y") {
      q = parseInt(temp);
      temp = "";
      createLevel();
      for (w = _j = 0; 0 <= q ? _j < q : _j > q; w = 0 <= q ? ++_j : --_j) {
        ttt = levelId - 1;
        ww = w + 1;
        $("#level" + ttt).append($(nodeCode).attr("id", nodeClass + ttt + 'x' + ww).attr("class", nodeClass));
      }
    } else {
      temp += nvar[i];
    }
  }
  deleteLevelButton();
  addLevelButton();
  deleteNodeButtons();
  addNodeButtons();
  writeId();
  tabIndexOrder();
  textArray2 = JSON.parse($.cookie("nodesContents"));
  nodesNames2 = JSON.parse($.cookie("nodesNames"));
  levelNames2 = JSON.parse($.cookie("levelNames"));
  $(".level").each(function() {
    return $(this).children(".levelButtonsWrapper").children(".levelText").children(".levelName").val(levelNames2.pop());
  });
  return $(".node").each(function() {
    $(this).children(".nodeCanvas").children("textarea").val(textArray2.pop());
    return $(this).children(".buttons").children(".nodeName").val(nodesNames2.pop());
  });
};

doneButtonClick = function() {
  manageInput(false);
  $(".nodeName").animate({
    "left": "-=140px"
  });
  $(".nodeName").animate({
    "top": "-=23px"
  });
  $(".levelName").animate({
    "top": "-=20px"
  });
  $(".nodeText").css({
    background: "#EBEBE4"
  });
  $(".nodeName").css({
    background: "#EBEBE4"
  });
  $(".levelName").css({
    background: "#EBEBE4"
  });
  if ($("#checkboxAutosave").is(":checked")) {
    return saveNodes();
  }
};

backButtonClick = function() {
  var i, keys, size;
  $(".level").css({
    "bottom": "0px"
  });
  $("#backButton").fadeOut();
  $("#startButton").fadeOut();
  $("#csvExportButton").fadeOut();
  manageInput(true);
  $(".line").remove();
  $(".linetemp").remove();
  $(".nodeName").animate({
    "left": "+=140px"
  });
  $(".nodeName").animate({
    "top": "+=23px"
  });
  setTimeout((function() {
    return $(".levelName").animate({
      "top": "+=20px"
    });
  }), 600);
  $(".nodeText").css({
    background: "#FFF",
    "border-color": "#777"
  });
  $(".nodeName").css({
    background: "#FFF"
  });
  $(".levelName").css({
    background: "#FFF"
  });
  $(".nodeText").css;
  array = [];
  from = '';
  to = '';
  levelFrom = '';
  levelTo = '';
  nodeFromNumber = '';
  nodeToNumber = '';
  x1 = 0;
  y1 = 0;
  x2 = 0;
  y2 = 0;
  thinLine = 0;
  fromArray = [];
  toArray = [];
  a = 0;
  coolArray = [[]];
  wtf = false;
  clickButtonActive = false;
  linkButton = '';
  firstClickLink = '';
  widthSlider = [];
  i = 0;
  size = Object.size(array);
  keys = Object.keys(array);
  while (i < size) {
    delete array[keys[i]];
    i++;
  }
  if ($(".outputContainer").length) {
    $(".outputContainer").remove();
  }
  return writeMessage("-=SCRATCH=-");
};

removeLevelButtonClick = function(button) {
  var id;
  if ($("#checkboxConfirmation").is(":checked")) {
    return deleteLevel(button);
  } else {
    id = getParentParentParentId(button);
    $(".deleteLevelModal").modal();
    return $(".deleteLevelModal").children(".hiddenInfo").val(id);
  }
};

editButtonClick = function(button) {
  var id, nodeContent;
  id = getParentParentId(button);
  root.editingNode = id;
  nodeContent = $("#" + id).children(".nodeCanvas").children("textarea").val();
  $(".editModal").modal();
  return $("#editModalTextbox").val(nodeContent);
};

addFirstNodeButtonClick = function(button) {
  var bar, id;
  id = getParentParentParentId(button);
  bar = id.substring(5);
  $("#" + levelClass + bar).append($(nodeCode).attr("id", nodeClass + bar + "x1").attr("class", nodeClass));
  $("#" + levelClass + bar + " .nodeEmpty").remove();
  deleteNodeButtons();
  addNodeButtons();
  writeId();
  return tabIndexOrder();
};

addNodeButtonClick = function(button) {
  var arrayR;
  arrayR = parseId(getParentParentId(button));
  arrayR[1]++;
  $("#level" + arrayR[0]).append($(nodeCode).attr("id", nodeClass + arrayR[0] + 'x' + arrayR[1]).attr("class", nodeClass));
  deleteNodeButtons();
  addNodeButtons();
  writeId();
  return tabIndexOrder();
};

delButtonClick = function(button) {
  var content, id;
  if ($("#checkboxConfirmation").is(":checked")) {
    return deleteNode(button);
  } else {
    id = getParentParentId(button);
    content = $("#" + id).children(".nodeCanvas").children("textarea").val();
    if (content.length > 0) {
      $(".deleteNodeModal").modal();
      return $(".deleteNodeModal").children(".hiddenInfo").val(id);
    } else {
      return deleteNode(button);
    }
  }
};

manageInput = function(flag) {
  if (flag) {
    $(".levelName").prop('readonly', false);
    $(".nodeName").prop('readonly', false);
    $(".nodeText").prop('readonly', false);
    $('.linkButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.dndButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.deleteLinkButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.deleteDragButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.levelDragArea').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.levelDragButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('#outputSlider').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addNodeButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addFirstNode').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addLevelButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    setTimeout((function() {
      $("#doneButton").fadeIn();
      $("#saveNodesButton").fadeIn();
      $("#loadNodesButton").fadeIn();
      $("#linesControlText").fadeOut();
      $("#linesControl").fadeOut();
      $('.linkButton').css({
        visibility: "hidden"
      });
      $('.dndButton').css({
        visibility: "hidden"
      });
      $('.deleteLinkButton').css({
        visibility: "hidden"
      });
      $('.deleteDragButton').css({
        visibility: "hidden"
      });
      $('.levelDragArea').css({
        visibility: "hidden"
      });
      $('.levelDragButton').css({
        visibility: "hidden"
      });
      return $('#outputSlider').css({
        visibility: "hidden"
      });
    }), 400);
    return setTimeout((function() {
      $('.removeLevelButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      return $('.editButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
    }), 900);
  } else {
    $("#doneButton").fadeOut();
    $('#saveNodesButton').fadeOut();
    $('#loadNodesButton').fadeOut();
    $(".levelName").prop('readonly', true);
    $(".nodeName").prop('readonly', true);
    $(".nodeText").prop('readonly', true);
    $('.removeLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addNodeButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addFirstNode').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.editButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.levelDragArea').first().css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.levelDragButton').last().css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    return setTimeout((function() {
      $("#backButton").fadeIn();
      $("#startButton").fadeIn();
      $("#linesControlText").fadeIn();
      $("#linesControl").fadeIn();
      $('.linkButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.dndButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.deleteLinkButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.deleteDragButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.levelDragArea').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.levelDragButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.removeLevelButton').css({
        visibility: "hidden"
      });
      $('.delButton').css({
        visibility: "hidden"
      });
      $('.addNodeButton').css({
        visibility: "hidden"
      });
      $('.addFirstNode').css({
        visibility: "hidden"
      });
      $('.addLevelButton').css({
        visibility: "hidden"
      });
      $('.editButton').css({
        visibility: "hidden"
      });
      $('.levelDragArea').first().css({
        visibility: "hidden"
      });
      return $('.levelDragButton').last().css({
        visibility: "hidden"
      });
    }), 400);
  }
};

createLine = function(x1, y1, x2, y2, temp, from, to) {
  var angle, asd, color, colorInt, length, line, lineClass, transform;
  if (from == null) {
    from = "";
  }
  if (to == null) {
    to = "";
  }
  if (temp) {
    lineClass = "linetemp";
    color = "black";
  } else {
    lineClass = "line";
    colorInt = getRandomInt(0, 4);
    switch (colorInt) {
      case 0:
        color = "red";
        break;
      case 1:
        color = "blue";
        break;
      case 2:
        color = "green";
        break;
      case 3:
        color = "purple";
        break;
      case 4:
        color = "orange";
    }
  }
  y1 = y1 - $(".levelContanter").scrollTop();
  x1 = x1 - $(".levelContanter").scrollLeft();
  length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  transform = "rotate(" + angle + "deg)";
  if (temp && y2 < y1) {
    y1 = $("#node" + from).offset().top;
    y1 -= 100;
  }
  if (temp) {
    asd = parseInt($(".level").css("bottom"));
    asd++;
    asd = asd + "px";
    $(".level").css({
      "bottom": asd
    });
  }
  line = $("<div>").prependTo(".levelContanter").addClass(lineClass).attr('id', from + "y" + to).css({
    position: "relative",
    transform: transform,
    background: color
  }).width(length).offset({
    left: x1,
    top: y1
  });
  return line;
};

fixLines = function() {
  $(".level").css({
    "bottom": $('.line').length + "px"
  });
  return $(".line").each(function() {
    var hh, ida;
    ida = $(this).attr('id').split("y");
    hh = $("#node" + ida[0]).offset().top;
    hh += 100;
    return $(this).css("top", hh);
  });
};

fixLines2 = function() {
  return $(".line").each(function() {
    var asd;
    asd = parseInt($(this).css("top"));
    asd -= $(".line").length;
    asd = asd + "px";
    return $(this).css("top", asd);
  });
};

dragStartLevel = function(ev) {
  ev.dataTransfer.setData('Text', "asd");
  fromL = ev.target.parentNode.parentNode.id;
  from = "l2l";
  return linkButton = "levelMerge";
};

dropLevel = function(ev) {
  var l2;
  ev.preventDefault();
  ev.target.style.background = "#fff";
  l2 = ev.target.parentNode.parentNode.id;
  if (fromL.charAt(0) === "l") {
    $("#" + fromL + " .node").each(function() {
      var foo, height, id, left, offset, top, width;
      id = $(this).context.id;
      foo = parseId(id);
      nodeFromNumber = foo[1];
      offset = $("#" + id).offset();
      width = $("#" + id).width();
      height = $("#" + id).height();
      top = offset.top;
      left = offset.left;
      x1 = left + width - 92;
      y1 = top + height / 2;
      from = id.substring(4);
      return $("#" + l2).children(".node").each(function() {
        id = $(this).context.id;
        foo = parseId(id);
        nodeToNumber = foo[1];
        offset = $("#" + id).offset();
        height = $("#" + id).height();
        top = offset.top;
        left = offset.left;
        x2 = left + 7;
        y2 = top + (height / 2);
        to = id.substring(4);
        createLine(x1, y1, x2, y2, false, from, to);
        if (array[from] === void 0) {
          array[from] = [];
        }
        if (array[from].indexOf(to) > -1) {
          return writeMessage("you do have this nodes linked");
        } else {
          array[from].push(to);
          return array[from].sort();
        }
      });
    });
  } else {
    $("#" + l2).children(".node").each(function() {
      var foo, height, id, left, offset, top;
      id = $(this).context.id;
      foo = parseId(id);
      nodeToNumber = foo[1];
      console.log(nodeToNumber);
      offset = $("#" + id).offset();
      height = $("#" + id).height();
      top = offset.top;
      left = offset.left;
      x2 = left + 7;
      y2 = top + (height / 2);
      to = id.substring(4);
      createLine(x1, y1, x2, y2, false, from, to);
      if (array[from] === void 0) {
        array[from] = [];
      }
      if (array[from].indexOf(to) > -1) {
        return writeMessage("you do have this nodes linked");
      } else {
        array[from].push(to);
        return array[from].sort();
      }
    });
  }
  console.log(array);
  fixLines();
  return fixLines2();
};

dragHoverLevel = function(ev) {
  var id;
  ev.preventDefault();
  id = ev.target.parentNode.parentNode.id;
  return ev.target.style.background = "blue";
};

dragLeaveLevel = function(ev) {
  ev.preventDefault();
  return ev.target.style.background = "#fff";
};

dragStart = function(ev) {
  var foo, height, id, left, offset, top, width;
  ev.dataTransfer.setData('Text', this.id);
  id = ev.target.parentNode.parentNode.id;
  linkButton = ev.target.className;
  foo = parseId(id);
  nodeFromNumber = foo[1];
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  return y1 = top + height / 2;
};

dragHover = function(ev) {
  var foo, foor, height, id, left, offset, top;
  ev.preventDefault();
  if (from === "l2l") {
    ev.target.style.background = "red";
    return;
  }
  id = ev.target.parentNode.parentNode.id;
  foor = id.substring(4);
  foo = parseId(id);
  nodeToNumber = foo[1];
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x2 = left + 7;
  y2 = top + (height / 2);
  if (levelTo > levelFrom && thinLine === 0) {
    ev.target.style.background = "#CCCCFF";
    createLine(x1, y1, x2, y2, true, from, foor);
    thinLine++;
  }
  if (levelTo <= levelFrom) {
    return ev.target.style.background = "#FFAAAA";
  }
};

dragLeave = function(ev) {
  var asd;
  defaultBackground(ev);
  $(".linetemp").remove();
  asd = parseInt($(".level").css("bottom"));
  asd--;
  asd = asd + "px";
  $(".level").css({
    "bottom": asd
  });
  return thinLine = 0;
};

deleteLink = function(ev) {
  var el, selector;
  ev.preventDefault();
  defaultBackground(ev);
  to = ev.target.parentNode.parentNode.id.substring(4);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  if (levelTo > levelFrom) {
    selector = "#" + from + "y" + to;
    if ($(selector).length > 0) {
      el = array[from].indexOf(to);
      array[from].splice(el, 1);
      if (array[from].length === 0) {
        delete array[from];
      }
      $(selector).remove();
      $("#node" + to).children(".nodeCanvas").children(".nodeText").css({
        "border-color": "#777"
      });
    } else {
      writeError("this nodes are not linked");
    }
  } else {
    writeError("deleting is allowed only to higher level node");
  }
  $(".linetemp").remove();
  return thinLine = 0;
};

addLink = function(ev) {
  ev.preventDefault();
  defaultBackground(ev);
  to = ev.target.parentNode.parentNode.id.substring(4);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  if (levelTo > levelFrom) {
    if (array[from] === void 0) {
      array[from] = [];
    }
    if (array[from].indexOf(to) > -1) {
      writeMessage("you do have this nodes linked");
    } else {
      array[from].push(to);
      array[from].sort();
      writeMessage("linked " + from + " to " + to);
      createLine(x1, y1, x2, y2, false, from, to);
      $("#node" + to).children(".nodeCanvas").children(".nodeText").css({
        "border-color": "red"
      });
    }
    $(".linetemp").remove();
    return thinLine = 0;
  } else {
    return writeError("can not link to current or previous level");
  }
};

drop = function(ev) {
  console.log(array);
  if (linkButton === "dndButton") {
    addLink(ev);
  } else if (linkButton === "deleteDragButton") {
    deleteLink(ev);
  } else if (linkButton === "linkButton") {
    addLink(ev);
  } else if (linkButton === "levelMerge") {
    writeMessage("you can not link level to node");
    $("#node" + ev.target.parentNode.parentNode.id.substring(4)).children(".nodeCanvas").children(".nodeText").css({
      "background": "#EBEBE4"
    });
  } else if (linkButton === "deleteLinkButton") {
    deleteLink(ev);
  }
  fixLines();
  return fixLines2();
};

clickStart = function(ev) {
  var height, id, left, offset, top, width;
  console.log("LALA1");
  id = ev.target.parentNode.parentNode.id;
  console.log("LALA2");
  nodeFromNumber = id.substring(6);
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  y1 = top + height / 2;
  linkButton = ev.target.className;
  clickButtonActive = true;
  console.log(clickButtonActive);
  return false;
};

merge = function(j) {
  var check, count, k, position, q, thisArray, _results;
  coolArray[a].push(toArray[j]);
  console.log("pushing " + toArray[j] + ". now " + coolArray[a] + " in [" + a + "]");
  q = 0;
  count = 0;
  position = [];
  check = toArray[j];
  while (q < fromArray.length) {
    if (check === fromArray[q]) {
      count++;
      position.push(q);
    }
    q++;
  }
  if (count === 0) {

  } else if (count === 1) {
    return merge(position[0]);
  } else {
    k = 0;
    _results = [];
    while (k < count) {
      if (k !== count - 1) {
        thisArray = coolArray[a].slice();
        coolArray[a + 1] = coolArray[a].slice();
      } else {
        merge(position[k]);
        wtf = true;
        break;
      }
      merge(position[k]);
      a++;
      if (wtf) {
        coolArray[a] = thisArray;
        wtf = false;
      }
      _results.push(k++);
    }
    return _results;
  }
};

lala = function(first, second) {
  var arrayR, i, j, string;
  arrayR = [];
  if (first.length === 0 && second.length === 0) {

  } else if (first.length === 0 && second.length > 0) {
    j = 0;
    while (j < second.length) {
      string = second[j];
      arrayR.push(string);
      j++;
    }
  } else if (first.length > 0 && second.length === 0) {
    i = 0;
    while (i < first.length) {
      string = first[i];
      arrayR.push(string);
      i++;
    }
  } else {
    i = 0;
    while (i < first.length) {
      j = 0;
      while (j < second.length) {
        string = first[i] + " " + second[j];
        arrayR.push(string);
        j++;
      }
      i++;
    }
  }
  return arrayR;
};

combine = function(array) {
  var arr1, arr2, i, j, k, result, temp, temp1, temp2;
  i = 0;
  result = [];
  while (i < array.length) {
    temp = [];
    j = 0;
    while (j < array[i].length - 1) {
      if ((temp != null ? temp.length : void 0) === 0) {
        temp1 = $("#node" + array[i][j]).find(".nodeText").val().split(/\n/);
        arr1 = [];
        k = 0;
        while (k < temp1.length) {
          if (/\S/.test(temp1[k])) {
            arr1.push($.trim(temp1[k]));
          }
          k++;
        }
      } else {
        arr1 = temp;
      }
      temp2 = $("#node" + array[i][j + 1]).find(".nodeText").val().split(/\n/);
      arr2 = [];
      k = 0;
      while (k < temp2.length) {
        if (/\S/.test(temp2[k])) {
          arr2.push($.trim(temp2[k]));
        }
        k++;
      }
      temp = lala(arr1, arr2);
      j++;
    }
    i++;
    result.push(temp);
  }
  return result;
};

start = function() {
  var firstNodes, i, j, keys, outputArrow, outputNameString, size, unique, uniqueFirstNodes;
  size = Object.size(array);
  keys = Object.keys(array);
  firstNodes = [];
  uniqueFirstNodes = [];
  i = 0;
  while (i < size) {
    j = 0;
    while (j < array[keys[i]].length) {
      fromArray.push(keys[i]);
      toArray.push(array[keys[i]][j]);
      j++;
    }
    i++;
  }
  i = 0;
  while (i < fromArray.length) {
    unique = true;
    j = 0;
    while (j < toArray.length) {
      if (fromArray[i] === toArray[j]) {
        unique = false;
        break;
      }
      j++;
    }
    if (unique) {
      firstNodes.push(fromArray[i]);
    }
    i++;
  }
  $.each(firstNodes, function(i, el2) {
    if ($.inArray(el2, uniqueFirstNodes) === -1) {
      return uniqueFirstNodes.push(el2);
    }
  });
  uniqueFirstNodes.sort();
  i = 0;
  while (i < uniqueFirstNodes.length) {
    j = 0;
    while (j < fromArray.length) {
      if (uniqueFirstNodes[i] === fromArray[j]) {
        coolArray[a] = [];
        coolArray[a].push(fromArray[j]);
        console.log("beginning: pushing " + fromArray[j] + ". now " + coolArray[a] + " in [" + a + "]");
        merge(j);
        a++;
      }
      j++;
    }
    i++;
  }
  console.log(coolArray);
  outputArrow = combine(coolArray);
  i = 0;
  if ($("#checkboxSortOutput").is(":checked")) {
    while (i < outputArrow.length) {
      outputArrow[i].sort();
      i++;
    }
  }
  $("body").append("<div class='outputContainer'></div>");
  i = 0;
  while (i < outputArrow.length) {
    $(".outputContainer").append($(outputNodeCode).attr("id", "output" + i));
    $("#output" + i).children("textarea").val(outputArrow[i].join("\n"));
    j = 0;
    outputNameString = '';
    while (j < coolArray[i].length) {
      if ($("#node" + coolArray[i][j]).children(".buttons").children(".nodeName").val() === "") {
        outputNameString += coolArray[i][j];
      } else {
        outputNameString += $("#node" + coolArray[i][j]).children(".buttons").children(".nodeName").val();
      }
      if (j !== coolArray[i].length - 1) {
        outputNameString += " ";
      }
      j++;
    }
    $("#output" + i).children(".outputNodeName").html(outputNameString);
    i++;
  }
  $(".coffeeModal").modal('hide');
  if ($("#checkboxPerformance").is(":checked")) {
    return $(".levelContanter").remove();
  } else {
    return $.scrollTo(".outputContainer");
  }
};

$(document).on("click", "#doneButton", function() {
  return doneButtonClick();
});

$(document).on("click", "#backButton", function() {
  return backButtonClick();
});

$(document).on("click", "#startButton", function() {
  if ($("#checkboxPerformance").is(":checked")) {
    $(".levelButtonsWrapper").remove();
    $("#linesControl").remove();
    $("#linesControlText").remove();
    $(".addNodeButtonWrapper").remove();
    $(".linkButton").remove();
    $(".dndButton").remove();
    $(".deleteLinkButton").remove();
    $(".deleteDragButton").remove();
    $(".editButton").remove();
    $(".delButton").remove();
    $(".line").remove();
    $("#backButton").remove();
    $("#settingsButton").remove();
    $(".nodeName").hide();
    $(".matrixPosition").hide();
  }
  $(".coffeeModal").modal();
  setTimeout((function() {
    return start();
  }), 500);
  $('#outputSlider').css({
    opacity: 0.0,
    visibility: "visible"
  }).animate({
    opacity: 1.0
  });
  $("#startButton").fadeOut();
  return $("#csvExportButton").fadeIn();
});

$(document).on("click", "#csvExportButton", function() {
  var csvString;
  csvString = "";
  if ($("#checkboxPerformance").is(":checked")) {
    $(".levelContanter").remove();
    $(".lastLevelModal").remove();
    $(".editModal").remove();
    $(".deleteNodeModal").remove();
    $(".deleteLevelModal").remove();
    $(".coffeeModal").remove();
    $(".controlPanel").remove();
    $("#outputSlider").remove();
  }
  $(".outputNodeName").each(function(i) {
    csvString += $(this).text();
    csvString += ",";
    csvString += $(this).next(".outputNodeContent").val().replace(/\n/g, ",");
    if (i !== $(".outputNodeName").length - 1) {
      csvString += "\n";
    }
    if ($("#checkboxPerformance").is(":checked")) {
      return $(".outputNodeName").parent().get(i).remove();
    }
  });
  if (csvString.length > 1550396) {
    return writeError("sorry, can not generate .csv file, too big size, work manual");
  } else {
    a = document.createElement("a");
    a.href = "data:attachment/csv," + encodeURIComponent(csvString);
    a.target = "_blank";
    a.download = "comgen2 " + getTime(false) + ".csv";
    document.body.appendChild(a);
    return a.click();
  }
});

$(document).on("click", ".addNodeButton", function() {
  return addNodeButtonClick($(this));
});

$(document).on("click", ".addFirstNode", function() {
  return addFirstNodeButtonClick($(this));
});

$(document).on("click", ".delButton", function() {
  return delButtonClick($(this));
});

$(document).on("click", "#deleteNodeYes", function() {
  var id;
  id = $(".deleteNodeModal").children(".hiddenInfo").val();
  return deleteNode(id, true);
});

$(document).on("click", "#deleteLevelYes", function() {
  var id;
  id = $(".deleteLevelModal").children(".hiddenInfo").val();
  return deleteLevel(id, true);
});

$(document).on("click", ".removeLevelButton", function() {
  return removeLevelButtonClick($(this));
});

$(document).on("click", "#editModalDoneButton", function() {
  var newContent, newContentArray, parent;
  parent = $(this).parent();
  newContent = $(parent).children("#editModalTextbox").val();
  if ($("#allLO").is(':checked')) {
    newContent = newContent.toLowerCase();
  }
  if ($("#allUP").is(':checked')) {
    newContent = newContent.toUpperCase();
  }
  if ($("#wordsUP").is(':checked')) {
    newContent = uppercaseFirstLetterAllWords(newContent);
  }
  if ($("#wordsLO").is(':checked')) {
    newContent = lowercaseFirstLetterAllWords(newContent);
  }
  if ($("#stringUP").is(':checked')) {
    newContent = newContent.toLowerCase();
    newContentArray = newContent.split('\n');
    newContent = '';
    $.each(newContentArray, function(i) {
      newContentArray[i] = uppercaseFirstLetter(newContentArray[i]);
      newContent += newContentArray[i];
      if (i !== newContentArray.length - 1) {
        return newContent += '\n';
      }
    });
  }
  if ($("#stringLO").is(':checked')) {
    newContent = newContent.toUpperCase();
    newContentArray = newContent.split('\n');
    newContent = '';
    $.each(newContentArray, function(i) {
      newContentArray[i] = lowercaseFirstLetter(newContentArray[i]);
      newContent += newContentArray[i];
      if (i !== newContentArray.length - 1) {
        return newContent += '\n';
      }
    });
  }
  $("#" + root.editingNode).children(".nodeCanvas").children("textarea").val(newContent);
  $('.editModal').modal('hide');
  return $("input[name=opt]:radio").removeAttr("checked");
});

$(document).on("keydown", "#editModalTextbox", function(e) {
  if ((e.metaKey && e.keyCode === 13) || (e.ctrlKey && e.keyCode === 13)) {
    return $("#editModalDoneButton").click();
  }
});

$(document).on("click", ".editButton", function() {
  return editButtonClick($(this));
});

$(document).on("click", "#settingsButton", function() {
  return $(".settingsModal").modal();
});

$(document).on("click", ".addLevelButton", function() {
  addFirstNode(createLevel());
  deleteNodeButtons();
  addNodeButtons();
  deleteLevelButton();
  addLevelButton();
  writeId();
  return tabIndexOrder();
});

$(document).ready(function() {
  addFirstNode(createLevel());
  addFirstNode(createLevel());
  addFirstNode(createLevel());
  writeId();
  tabIndexOrder();
  return writeMessage("welcome to comgen2 " + version);
});

$(window).load(function() {
  addNodeButtons();
  addLevelButton();
  widthSlider = new slider("outputSlider", 400, 10, 99, 0);
  return $("#outputSlider_slider").prepend("<p class='sliderText'>width of output window</p>");
});

$(document).on("mouseenter", ".nodeCanvas", function(event) {
  var bar, foo, id;
  if (clickButtonActive) {
    return dragHover(event);
  } else {
    $(".line").css({
      visibility: "hidden"
    });
    id = event.target.parentNode.parentNode.id;
    foo = id.substring(4);
    bar = 'div.line[id^="' + foo + 'y"]';
    return $(bar).css({
      visibility: "visible"
    });
  }
});

$(document).on("mouseleave", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    return dragLeave(event);
  } else {
    return $(".line").css({
      visibility: "visible"
    });
  }
});

$(document).keyup(function(e) {
  if (e.keyCode === 27) {
    clickButtonActive = false;
    $(".linetemp").remove();
    return $(".nodeText").css({
      background: "#EBEBE4"
    });
  }
});

$(document).on("click", ".nodeCanvas", function(ev) {
  if (clickButtonActive) {
    clickButtonActive = false;
    return drop(ev);
  }
});

$(document).on("click", "#saveNodesButton", function() {
  return saveNodes();
});

$(document).on("click", "#loadNodesButton", function() {
  return loadNodes();
});

$(document).on("click", "#linesUp", function() {
  return linesUp();
});

$(document).on("click", "#linesDown", function() {
  return linesDown();
});
