var a, addFirstNode, addFirstNodeButtonClick, addFirstNodeCode, addLevelButton, addLevelButtonCode, addLink, addNodeButtonClick, addNodeButtonCode, addNodeButtons, array, backButtonClick, clickButtonActive, clickStart, combine, coolArray, createLevel, createLine, defaultBackground, delButtonClick, deleteId, deleteLevel, deleteLevelButton, deleteLink, deleteNode, deleteNodeButtons, doneButtonClick, dragHover, dragLeave, dragStart, drop, editButtonClick, firstClickLink, from, fromArray, getParentId, getParentParentId, getParentParentParentId, getRandomInt, getTime, lala, levelClass, levelCode, levelContainerClass, levelFrom, levelId, levelTo, linkButton, manageInput, merge, nodeClass, nodeCode, nodeFromNumber, nodeToNumber, outputNodeCode, parseId, removeLevelButtonClick, root, sortLevelId, sortNodeId, start, tabIndexOrder, thinLine, to, toArray, writeError, writeId, writeMessage, wtf, x1, x2, y1, y2;

levelId = 1;

levelClass = "level";

levelContainerClass = "levelContanter";

nodeClass = "node";

array = [];

from = '';

to = '';

levelFrom = '';

levelTo = '';

nodeFromNumber = '';

nodeToNumber = '';

x1 = 0;

y1 = 0;

x2 = 0;

y2 = 0;

thinLine = 0;

fromArray = [];

toArray = [];

a = 0;

coolArray = [[]];

wtf = false;

clickButtonActive = false;

linkButton = '';

firstClickLink = '';

root = typeof exports !== "undefined" && exports !== null ? exports : this;

root.editingNode = '';

outputNodeCode = "<div class='outputNode' contentEditable> <textarea class='levelName'></textarea> </div>";

levelCode = "<div> <div class='levelButtonsWrapper'> <div class='removeLevelButtonWrapper'> <button class='removeLevelButton'>remove level</button> </div> <div class='levelText'> <input class='levelName' placeholder='level name'> </div> </div> </div>";

nodeCode = "<div> <div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'> <textarea class='nodeText'></textarea> </div> <div class='buttons'> <button class='linkButton' onclick='clickStart(event)'>click</button> <button class='dndButton' draggable='true' ondragstart='dragStart(event)'>drag</button> <button class='deleteLinkButton' onclick='clickStart(event)'>click delete</button> <button class='deleteDragButton' draggable='true' ondragstart='dragStart(event)'>drag delete</button> <button class='editButton'>edit</button> <input class='nodeName' placeholder='node name'> <div class='buttonBr'></div> <button class='delButton'>delete</button> </div> </div>";

addNodeButtonCode = "<div class='addNodeButtonWrapper'> <button class='addNodeButton'>add node</button> </div>";

addLevelButtonCode = "<div class='addLevelButtonWrapper'> <button class='addLevelButton'>add level</button> </div>";

addFirstNodeCode = "<div class='" + nodeClass + " nodeEmpty'> <div class='addNodeButtonWrapper'> <button class='addFirstNode'>add node</button> </div> </div>";

getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

getTime = function() {
  var hours, minutes, now, seconds, time;
  now = new Date();
  hours = now.getHours();
  minutes = now.getMinutes();
  seconds = now.getSeconds();
  return time = hours + ':' + minutes + ':' + seconds + ': ';
};

Object.size = function(obj) {
  var key, size;
  size = 0;
  key = void 0;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      size++;
    }
  }
  return size;
};

getParentParentParentId = function(item) {
  return item.parent().parent().parent().attr('id');
};

getParentParentId = function(item) {
  return item.parent().parent().attr('id');
};

getParentId = function(item) {
  return item.parent().attr('id');
};

addFirstNode = function(level) {
  var nodeId;
  nodeId = level + "x1";
  return $("#" + levelClass + level).append($(nodeCode).attr("id", nodeClass + nodeId).attr("class", nodeClass));
};

createLevel = function() {
  $("." + levelContainerClass).append($(levelCode).attr("id", levelClass + levelId).attr("class", levelClass));
  return levelId++;
};

parseId = function(id) {
  var foo;
  foo = id.substring(4);
  array = foo.split("x");
  return array;
};

sortNodeId = function(id) {
  array = parseId(id);
  return $("#" + levelClass + array[0] + " ." + nodeClass).each(function(i) {
    return $(this).attr('id', nodeClass + array[0] + 'x' + (i + 1));
  });
};

sortLevelId = function() {
  return $("." + levelClass).each(function(i) {
    $(this).attr('id', levelClass + (i + 1));
    return $(this).children("." + nodeClass).each(function(j) {
      return $(this).attr('id', nodeClass + (i + 1) + 'x' + (j + 1));
    });
  });
};

addNodeButtons = function() {
  return $("." + levelClass + " ." + nodeClass + ":last-child").each(function(i) {
    if (!($(this).hasClass("nodeEmpty"))) {
      return $(this).append(addNodeButtonCode);
    }
  });
};

deleteNodeButtons = function() {
  return $(".addNodeButtonWrapper").each(function(i) {
    if (!($(this).parent().hasClass("nodeEmpty"))) {
      return $(this).remove();
    }
  });
};

addLevelButton = function() {
  return $("." + levelClass + ":last-child .levelButtonsWrapper").append(addLevelButtonCode);
};

deleteLevelButton = function() {
  return $(".addLevelButtonWrapper").remove();
};

deleteId = function() {
  return $(".matrixPosition").remove();
};

writeId = function(node) {
  deleteId();
  return $("." + nodeClass).each(function(i) {
    var id;
    id = $(this).attr('id').substring(4);
    return $(this).children(".buttons").append("<span class='matrixPosition'>" + id + "</span>");
  });
};

tabIndexOrder = function() {
  var i;
  i = 1;
  $(".levelName").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
  $(".nodeName").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
  return $(".nodeText").each(function() {
    $(this).attr("tabindex", i);
    return i++;
  });
};

writeError = function(message) {
  return $(".console").prepend(getTime() + '<span style="color:red;">ERROR: </span>' + message + '<br>');
};

writeMessage = function(message) {
  return $(".console").prepend(getTime() + '<span style="color:#00AA00;">' + message + '</span><br>');
};

defaultBackground = function(ev) {
  return ev.target.style.background = "#EBEBE4";
};

deleteNode = function(node, a) {
  var id, nextNode;
  if (a) {
    id = node;
  } else {
    id = getParentParentId(node);
  }
  $("#" + id).remove();
  array = parseId(id);
  nextNode = parseInt(array[1]);
  nextNode++;
  if (array[1] === "1" && $('#' + nodeClass + array[0] + 'x' + nextNode).length === 0) {
    return $("#" + levelClass + array[0]).append(addFirstNodeCode);
  } else {
    sortNodeId(id);
    deleteNodeButtons();
    addNodeButtons();
    writeId();
    return tabIndexOrder();
  }
};

deleteLevel = function(level, a) {
  var bar, id;
  if (a) {
    id = level;
  } else {
    id = getParentParentParentId($(level));
  }
  bar = id.substring(5);
  if (bar === "1" && $('#' + levelClass + "2").length === 0) {
    return $(".lastLevelModal").modal();
  } else {
    $("#" + id).remove();
    levelId--;
    sortLevelId();
    deleteLevelButton();
    addLevelButton();
    writeId();
    return tabIndexOrder();
  }
};

doneButtonClick = function() {
  $("#doneButton").fadeOut();
  manageInput(false);
  $(".nodeName").animate({
    "left": "-=140px"
  });
  $(".nodeName").animate({
    "top": "-=22px"
  });
  $(".levelName").animate({
    "top": "-=20px"
  });
  $(".nodeText").css({
    background: "#EBEBE4"
  });
  $(".nodeName").css({
    background: "#EBEBE4"
  });
  return $(".levelName").css({
    background: "#EBEBE4"
  });
};

backButtonClick = function() {
  var i, keys, size, _results;
  $("#backButton").fadeOut();
  $("#startButton").fadeOut();
  manageInput(true);
  $(".line").remove();
  $(".linetemp").remove();
  $(".nodeName").animate({
    "left": "+=140px"
  });
  $(".nodeName").animate({
    "top": "+=22px"
  });
  setTimeout((function() {
    return $(".levelName").animate({
      "top": "+=20px"
    });
  }), 600);
  $(".nodeText").css({
    background: "#FFF"
  });
  $(".nodeName").css({
    background: "#FFF"
  });
  $(".levelName").css({
    background: "#FFF"
  });
  thinLine = 0;
  fromArray = [];
  toArray = [];
  a = 0;
  coolArray = [[]];
  wtf = false;
  i = 0;
  size = Object.size(array);
  keys = Object.keys(array);
  _results = [];
  while (i < size) {
    delete array[keys[i]];
    _results.push(i++);
  }
  return _results;
};

removeLevelButtonClick = function(button) {
  var id;
  if ($("#checkboxConfirmation").is(":checked")) {
    return deleteLevel(button);
  } else {
    id = getParentParentParentId(button);
    $(".deleteLevelModal").modal();
    return $(".deleteLevelModal").children(".hiddenInfo").val(id);
  }
};

editButtonClick = function(button) {
  var id, nodeContent;
  id = getParentParentId(button);
  root.editingNode = id;
  nodeContent = $("#" + id).children(".nodeCanvas").children("textarea").val();
  $(".editModal").modal();
  return $("#editModalTextbox").val(nodeContent);
};

addFirstNodeButtonClick = function(button) {
  var bar, id;
  id = getParentParentParentId(button);
  bar = id.substring(5);
  $("#" + levelClass + bar).append($(nodeCode).attr("id", nodeClass + bar + "x1").attr("class", nodeClass));
  $("#" + levelClass + bar + " .nodeEmpty").remove();
  deleteNodeButtons();
  addNodeButtons();
  writeId();
  return tabIndexOrder();
};

addNodeButtonClick = function(button) {
  array = parseId(getParentParentId(button));
  array[1]++;
  $("#level" + array[0]).append($(nodeCode).attr("id", nodeClass + array[0] + 'x' + array[1]).attr("class", nodeClass));
  deleteNodeButtons();
  addNodeButtons();
  writeId();
  return tabIndexOrder();
};

delButtonClick = function(button) {
  var content, id;
  if ($("#checkboxConfirmation").is(":checked")) {
    return deleteNode(button);
  } else {
    id = getParentParentId(button);
    content = $("#" + id).children(".nodeCanvas").children("textarea").val();
    if (content.length > 0) {
      $(".deleteNodeModal").modal();
      return $(".deleteNodeModal").children(".hiddenInfo").val(id);
    } else {
      return deleteNode(button);
    }
  }
};

manageInput = function(flag) {
  if (flag) {
    $(".levelName").attr('disabled', false);
    $(".nodeName").attr('disabled', false);
    $(".nodeText").attr('disabled', false);
    $('.linkButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.dndButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.deleteLinkButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.deleteDragButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addNodeButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addFirstNode').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addLevelButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    setTimeout((function() {
      $("#doneButton").fadeIn();
      $('.linkButton').css({
        visibility: "hidden"
      });
      $('.dndButton').css({
        visibility: "hidden"
      });
      $('.deleteLinkButton').css({
        visibility: "hidden"
      });
      return $('.deleteDragButton').css({
        visibility: "hidden"
      });
    }), 400);
    return setTimeout((function() {
      $('.removeLevelButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      return $('.editButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
    }), 900);
  } else {
    $(".levelName").attr('disabled', true);
    $(".nodeName").attr('disabled', true);
    $(".nodeText").attr('disabled', true);
    $('.removeLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addNodeButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addFirstNode').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.editButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    return setTimeout((function() {
      $("#backButton").fadeIn();
      $("#startButton").fadeIn();
      $('.linkButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.dndButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.deleteLinkButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.deleteDragButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.removeLevelButton').css({
        visibility: "hidden"
      });
      $('.delButton').css({
        visibility: "hidden"
      });
      $('.addNodeButton').css({
        visibility: "hidden"
      });
      $('.addFirstNode').css({
        visibility: "hidden"
      });
      $('.addLevelButton').css({
        visibility: "hidden"
      });
      return $('.editButton').css({
        visibility: "hidden"
      });
    }), 400);
  }
};

createLine = function(x1, y1, x2, y2, temp, from, to) {
  var angle, color, colorInt, length, line, lineClass, transform;
  if (from == null) {
    from = "";
  }
  if (to == null) {
    to = "";
  }
  if (temp) {
    lineClass = "linetemp";
    color = "black";
  } else {
    lineClass = "line";
    colorInt = getRandomInt(0, 4);
    switch (colorInt) {
      case 0:
        color = "red";
        break;
      case 1:
        color = "blue";
        break;
      case 2:
        color = "green";
        break;
      case 3:
        color = "purple";
        break;
      case 4:
        color = "orange";
    }
  }
  length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  transform = "rotate(" + angle + "deg)";
  if (nodeToNumber < nodeFromNumber) {
    y1 = y2;
  }
  line = $("<div>").appendTo(".levelContanter").addClass(lineClass).attr('id', from + "y" + to).css({
    position: "relative",
    transform: transform,
    background: color
  }).width(length).offset({
    left: x1,
    top: y1
  });
  return line;
};

dragStart = function(ev) {
  var height, id, left, offset, top, width;
  id = ev.target.parentNode.parentNode.id;
  linkButton = ev.target.className;
  nodeFromNumber = id.substring(6);
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  return y1 = top + height / 2;
};

dragHover = function(ev) {
  var height, id, left, offset, top;
  ev.preventDefault();
  id = ev.target.parentNode.parentNode.id;
  nodeToNumber = id.substring(6);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x2 = left + 7;
  y2 = top + height;
  if (levelTo > levelFrom && thinLine === 0) {
    ev.target.style.background = "#CCCCFF";
    createLine(x1, y1, x2, y2, true);
    thinLine++;
  }
  if (levelTo <= levelFrom) {
    return ev.target.style.background = "#FFAAAA";
  }
};

dragLeave = function(ev) {
  defaultBackground(ev);
  $(".linetemp").remove();
  return thinLine = 0;
};

deleteLink = function(ev) {
  var el, selector;
  ev.preventDefault();
  defaultBackground(ev);
  to = ev.target.parentNode.parentNode.id.substring(4);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  if (levelTo > levelFrom) {
    selector = "#" + from + "y" + to;
    if ($(selector).length > 0) {
      el = array[from].indexOf(to);
      array[from].splice(el, 1);
      if (array[from].length === 0) {
        delete array[from];
      }
      $(selector).remove();
    } else {
      writeError("this nodes are not linked");
    }
  } else {
    writeError("deleting is allowed only to higher level node");
  }
  $(".linetemp").remove();
  return thinLine = 0;
};

addLink = function(ev) {
  ev.preventDefault();
  defaultBackground(ev);
  to = ev.target.parentNode.parentNode.id.substring(4);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  if (levelTo > levelFrom) {
    if (array[from] === void 0) {
      array[from] = [];
    }
    if (array[from].indexOf(to) > -1) {
      writeMessage("you do have this nodes linked");
    } else {
      array[from].push(to);
      array[from].sort();
      writeMessage("linked " + from + " to " + to);
      createLine(x1, y1, x2, y2, false, from, to);
    }
    $(".linetemp").remove();
    return thinLine = 0;
  } else {
    return writeError("can not link to current or previous level");
  }
};

drop = function(ev) {
  if (linkButton === "dndButton") {
    return addLink(ev);
  } else if (linkButton === "deleteDragButton") {
    return deleteLink(ev);
  } else if (linkButton === "linkButton") {
    return addLink(ev);
  } else if (linkButton === "deleteLinkButton") {
    return deleteLink(ev);
  }
};

clickStart = function(ev) {
  var height, id, left, offset, top, width;
  id = ev.target.parentNode.parentNode.id;
  nodeFromNumber = id.substring(6);
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  y1 = top + height / 2;
  linkButton = ev.target.className;
  return clickButtonActive = true;
};

merge = function(j) {
  var check, count, k, position, q, thisArray, _results;
  coolArray[a].push(toArray[j]);
  q = 0;
  count = 0;
  position = [];
  check = toArray[j];
  while (q < fromArray.length) {
    if (check === fromArray[q]) {
      count++;
      position.push(q);
    }
    q++;
  }
  if (count === 0) {

  } else if (count === 1) {
    return merge(position[0]);
  } else {
    k = 0;
    _results = [];
    while (k < count) {
      if (k !== count - 1) {
        thisArray = coolArray[a].slice();
        coolArray[a + 1] = coolArray[a].slice();
      } else {
        merge(position[k]);
        wtf = true;
        break;
      }
      merge(position[k]);
      a++;
      if (wtf) {
        coolArray[a] = thisArray;
        wtf = false;
      }
      _results.push(k++);
    }
    return _results;
  }
};

lala = function(first, second) {
  var i, j, string;
  array = [];
  if (first.length === 0 && second.length === 0) {

  } else if (first.length === 0 && second.length > 0) {
    j = 0;
    while (j < second.length) {
      string = second[j];
      array.push(string);
      j++;
    }
  } else if (first.length > 0 && second.length === 0) {
    i = 0;
    while (i < first.length) {
      string = first[i];
      array.push(string);
      i++;
    }
  } else {
    i = 0;
    while (i < first.length) {
      j = 0;
      while (j < second.length) {
        string = first[i] + " " + second[j];
        array.push(string);
        j++;
      }
      i++;
    }
  }
  return array;
};

combine = function(array) {
  var arr1, arr2, i, j, k, result, temp, temp1, temp2;
  i = 0;
  result = [];
  while (i < array.length) {
    temp = [];
    j = 0;
    while (j < array[i].length - 1) {
      if ((temp != null ? temp.length : void 0) === 0) {
        temp1 = $("#node" + array[i][j]).find(".nodeText").val().split(/\n/);
        arr1 = [];
        k = 0;
        while (k < temp1.length) {
          if (/\S/.test(temp1[k])) {
            arr1.push($.trim(temp1[k]));
          }
          k++;
        }
      } else {
        arr1 = temp;
      }
      temp2 = $("#node" + array[i][j + 1]).find(".nodeText").val().split(/\n/);
      arr2 = [];
      k = 0;
      while (k < temp2.length) {
        if (/\S/.test(temp2[k])) {
          arr2.push($.trim(temp2[k]));
        }
        k++;
      }
      temp = lala(arr1, arr2);
      j++;
    }
    i++;
    result.push(temp);
  }
  return result;
};

start = function() {
  var firstNodes, i, j, keys, outputArrow, size, unique, uniqueFirstNodes, _results;
  size = Object.size(array);
  keys = Object.keys(array);
  firstNodes = [];
  uniqueFirstNodes = [];
  i = 0;
  while (i < size) {
    j = 0;
    while (j < array[keys[i]].length) {
      fromArray.push(keys[i]);
      toArray.push(array[keys[i]][j]);
      j++;
    }
    i++;
  }
  i = 0;
  while (i < fromArray.length) {
    unique = true;
    j = 0;
    while (j < toArray.length) {
      if (fromArray[i] === toArray[j]) {
        unique = false;
        break;
      }
      j++;
    }
    if (unique) {
      firstNodes.push(fromArray[i]);
    }
    i++;
  }
  $.each(firstNodes, function(i, el) {
    if ($.inArray(el, uniqueFirstNodes) === -1) {
      return uniqueFirstNodes.push(el);
    }
  });
  uniqueFirstNodes.sort();
  i = 0;
  while (i < uniqueFirstNodes.length) {
    j = 0;
    while (j < fromArray.length) {
      if (uniqueFirstNodes[i] === fromArray[j]) {
        coolArray[a] = [];
        coolArray[a].push(fromArray[j]);
        merge(j);
        a++;
      }
      j++;
    }
    i++;
  }
  outputArrow = combine(coolArray);
  i = 0;
  if ($("#checkboxSortOutput").is(":checked")) {
    while (i < outputArrow.length) {
      outputArrow[i].sort();
      i++;
    }
  }
  i = 0;
  _results = [];
  while (i < outputArrow.length) {
    $(".outputContainer").append($(outputNodeCode).attr("id", "output" + i));
    $("#output" + i).children("textarea").val(outputArrow[i].join("\n"));
    _results.push(i++);
  }
  return _results;
};

$(document).on("click", "#doneButton", function() {
  return doneButtonClick();
});

$(document).on("click", "#backButton", function() {
  return backButtonClick();
});

$(document).on("click", "#startButton", function() {
  return start();
});

$(document).on("click", ".addNodeButton", function() {
  return addNodeButtonClick($(this));
});

$(document).on("click", ".addFirstNode", function() {
  return addFirstNodeButtonClick($(this));
});

$(document).on("click", ".delButton", function() {
  return delButtonClick($(this));
});

$(document).on("click", "#deleteNodeYes", function() {
  var id;
  id = $(".deleteNodeModal").children(".hiddenInfo").val();
  return deleteNode(id, true);
});

$(document).on("click", "#deleteLevelYes", function() {
  var id;
  id = $(".deleteLevelModal").children(".hiddenInfo").val();
  return deleteLevel(id, true);
});

$(document).on("click", ".removeLevelButton", function() {
  return removeLevelButtonClick($(this));
});

$(document).on("click", "#editModalDoneButton", function() {
  var newContent, parent;
  parent = $(this).parent();
  newContent = $(parent).children("#editModalTextbox").val();
  $("#" + root.editingNode).children(".nodeCanvas").children("textarea").val(newContent);
  return $('.editModal').modal('hide');
});

$(document).on("keydown", "#editModalTextbox", function(e) {
  if (e.metaKey && e.keyCode === 13) {
    $("#editModalDoneButton").click();
  }
  if (e.ctrlKey && e.keyCode === 13) {
    return $("#editModalDoneButton").click();
  }
});

$(document).on("click", ".editButton", function() {
  return editButtonClick($(this));
});

$(document).on("click", "#settingsButton", function() {
  return $(".settingsModal").modal();
});

$(document).on("click", ".addLevelButton", function() {
  addFirstNode(createLevel());
  deleteNodeButtons();
  addNodeButtons();
  deleteLevelButton();
  addLevelButton();
  writeId();
  return tabIndexOrder();
});

$(document).ready(function() {
  addFirstNode(createLevel());
  addFirstNode(createLevel());
  writeId();
  tabIndexOrder();
  return writeMessage("welcome to comgen2 beta v0.2.141126bbi");
});

$(window).load(function() {
  addNodeButtons();
  return addLevelButton();
});

$(document).on("mouseenter", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    return dragHover(event);
  }
});

$(document).on("mouseleave", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    return dragLeave(event);
  }
});

$(document).keyup(function(e) {
  if (e.keyCode === 27) {
    return clickButtonActive = false;
  }
});

$(document).on("click", ".nodeCanvas", function(ev) {
  if (clickButtonActive) {
    clickButtonActive = false;
    return drop(ev);
  }
});
