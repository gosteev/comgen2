# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                     global variables                      #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
levelId = 1;
levelClass = "level"
levelContainerClass = "levelContanter"
nodeClass = "node"
array = []
from = ''
to = ''
levelFrom = ''
levelTo = ''
nodeFromNumber = ''
nodeToNumber = ''
x1 = 0
y1 = 0
x2 = 0
y2 = 0
thinLine = 0
fromArray = []
toArray = []
a = 0
coolArray = [[]]
wtf = false
clickButtonActive = false
linkButton = ''
firstClickLink = ''

# very globar variables :)
root = exports ? this
root.editingNode = ''
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                            texts                          #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
outputNodeCode = "
<div class='outputNode' contentEditable>
	<textarea class='levelName'></textarea>
</div>
"
levelCode = "
<div>
	<div class='levelButtonsWrapper'>
		<div class='removeLevelButtonWrapper'>
		<button class='removeLevelButton'>remove level</button>
	</div>
	<div class='levelText'>
		<input class='levelName' placeholder='level name'>
	</div>
</div>
</div>
"
nodeCode = "
<div>
	<div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'>
		<textarea class='nodeText'></textarea>
	</div>
	<div class='buttons'>
		<button class='linkButton' onclick='clickStart(event)'>click</button>
		<button class='dndButton' draggable='true' ondragstart='dragStart(event)'>drag</button>
		<button class='deleteLinkButton' onclick='clickStart(event)'>click delete</button>
		<button class='deleteDragButton' draggable='true' ondragstart='dragStart(event)'>drag delete</button>
		<button class='editButton'>edit</button>
		<input class='nodeName' placeholder='node name'>
		<div class='buttonBr'></div>
		<button class='delButton'>delete</button>
	</div>
</div>
"
addNodeButtonCode = "
<div class='addNodeButtonWrapper'>
	<button class='addNodeButton'>add node</button>
</div>
"
addLevelButtonCode = "
<div class='addLevelButtonWrapper'>
	<button class='addLevelButton'>add level</button>
</div>
"
addFirstNodeCode = "
<div class='"+nodeClass+" nodeEmpty'>
<div class='addNodeButtonWrapper'>
	<button class='addFirstNode'>add node</button>
</div>
</div>
"
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                     helper functions                      #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
getRandomInt = (min, max) ->
	Math.floor(Math.random() * (max - min + 1)) + min

getTime = () ->
	now = new Date()
	hours = now.getHours()
	minutes = now.getMinutes()
	seconds = now.getSeconds()
	time = hours+':'+minutes+':'+seconds+': ';

Object.size = (obj) ->
	size = 0
	key = undefined
	for key of obj
		size++  if obj.hasOwnProperty(key)
	return size

getParentParentParentId = (item) ->
	item.parent().parent().parent().attr('id');

getParentParentId = (item) ->
	item.parent().parent().attr('id');

getParentId = (item) ->
	item.parent().attr('id');

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                      layout functions                     #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
addFirstNode = (level) ->
	nodeId = level + "x1"
	$("#"+levelClass+level).append $(nodeCode).attr("id",nodeClass+nodeId).attr("class",nodeClass)

createLevel = ->
	$("."+levelContainerClass).append $(levelCode).attr("id",levelClass+levelId).attr("class",levelClass)
	levelId++

parseId = (id) ->
	foo = id.substring(4) # because node has 4 characters
	array = foo.split("x");
	return array

sortNodeId = (id) ->
	array = parseId(id)
	$("#"+levelClass+array[0]+" ."+nodeClass).each (i) ->
		$(this).attr('id', nodeClass+array[0]+'x'+(i+1));

sortLevelId = ->
	$("."+levelClass).each (i) ->
		$(this).attr('id', levelClass+(i+1));
		$(this).children("."+nodeClass).each (j) ->
			$(this).attr('id', nodeClass+(i+1)+'x'+(j+1))

addNodeButtons = ->
	$("."+levelClass+" ."+nodeClass+":last-child").each (i) ->
		if !($(this).hasClass("nodeEmpty"))
			$(this).append(addNodeButtonCode)

deleteNodeButtons = ->
	$(".addNodeButtonWrapper").each (i) ->
		if !($(this).parent().hasClass("nodeEmpty"))
			$(this).remove()

addLevelButton = ->
	$("."+levelClass+":last-child .levelButtonsWrapper").append(addLevelButtonCode)

deleteLevelButton = ->
	$(".addLevelButtonWrapper").remove()

deleteId = ->
	$(".matrixPosition").remove()

writeId = (node) ->
	deleteId()
	$("."+nodeClass).each (i) ->
		id = $(this).attr('id').substring(4)
		$(this).children(".buttons").append "<span class='matrixPosition'>" + id + "</span>"

tabIndexOrder = ->
	i = 1
	$(".levelName").each ->
		$(this).attr "tabindex", i
		i++
	$(".nodeName").each ->
		$(this).attr "tabindex", i
		i++
	$(".nodeText").each ->
		$(this).attr "tabindex", i
		i++

writeError = (message) ->
	$(".console").prepend(getTime()+'<span style="color:red;">ERROR: </span>'+message+'<br>');

writeMessage = (message) ->
	$(".console").prepend(getTime()+'<span style="color:#00AA00;">'+message+'</span><br>');

defaultBackground = (ev) ->
	ev.target.style.background = "#EBEBE4"

deleteNode = (node,a) ->
	if a
		id = node
	else
		id = getParentParentId(node)
	$("#"+id).remove()
	array = parseId(id)
	nextNode = parseInt(array[1])
	nextNode++
	if array[1] == "1" && $('#'+nodeClass+array[0]+'x'+nextNode).length == 0
		$("#"+levelClass+array[0]).append(addFirstNodeCode);
	else
		sortNodeId(id)
		deleteNodeButtons()
		addNodeButtons()
		writeId()
		tabIndexOrder()

deleteLevel = (level,a) ->
	if a
		id = level
	else
		id = getParentParentParentId($(level))

	bar = id.substring(5) # because level has 5 characters
	if bar == "1" && $('#'+levelClass+"2").length == 0
		$(".lastLevelModal").modal()
	else
		$("#"+id).remove()
		levelId--
		sortLevelId()
		deleteLevelButton()
		addLevelButton()
		writeId()
		tabIndexOrder()

doneButtonClick = ->
	$("#doneButton").fadeOut()
	manageInput(false)
	$(".nodeName").animate({"left":"-=140px" });
	$(".nodeName").animate({"top":"-=22px" });
	$(".levelName").animate({"top":"-=20px" });
	$(".nodeText").css({background: "#EBEBE4"})
	$(".nodeName").css({background: "#EBEBE4"})
	$(".levelName").css({background: "#EBEBE4"})

backButtonClick = ->
	$("#backButton").fadeOut()
	$("#startButton").fadeOut()
	manageInput(true)
	$(".line").remove()
	$(".linetemp").remove()
	$(".nodeName").animate({"left":"+=140px" })
	$(".nodeName").animate({"top":"+=22px" })
	setTimeout (->
			$(".levelName").animate({"top":"+=20px" })
	), 600
	$(".nodeText").css({background: "#FFF"})
	$(".nodeName").css({background: "#FFF"})
	$(".levelName").css({background: "#FFF"})

	# null global variables
	thinLine = 0
	fromArray = []
	toArray = []
	a = 0
	coolArray = [[]]
	wtf = false

	# clearing array of links
	i = 0
	size = Object.size(array)
	keys = Object.keys(array)
	while i < size
		delete array[keys[i]]
		i++

removeLevelButtonClick = (button) ->
	if $("#checkboxConfirmation").is ":checked"
		deleteLevel(button)
	else
		id = getParentParentParentId(button)
		$(".deleteLevelModal").modal()
		$(".deleteLevelModal").children(".hiddenInfo").val(id)

editButtonClick = (button) ->
	id = getParentParentId(button)
	root.editingNode = id
	nodeContent = $("#"+id).children(".nodeCanvas").children("textarea").val()
	$(".editModal").modal()
	$("#editModalTextbox").val(nodeContent)

addFirstNodeButtonClick = (button) ->
	id = getParentParentParentId(button)
	bar = id.substring(5) # because level has 5 characters
	$("#"+levelClass+bar).append $(nodeCode).attr("id",nodeClass+bar+"x1").attr("class",nodeClass)
	$("#"+levelClass+bar+" .nodeEmpty").remove()
	deleteNodeButtons()
	addNodeButtons()
	writeId()
	tabIndexOrder()

addNodeButtonClick = (button) ->
	array = parseId(getParentParentId(button))
	array[1]++
	$("#level"+array[0]).append $(nodeCode).attr("id",nodeClass+array[0]+'x'+array[1]).attr("class",nodeClass)
	deleteNodeButtons()
	addNodeButtons()
	writeId()
	tabIndexOrder()

delButtonClick = (button) ->
	if $("#checkboxConfirmation").is ":checked"
		deleteNode(button)
	else
		id = getParentParentId(button)
		content = $("#"+id).children(".nodeCanvas").children("textarea").val()

		if content.length > 0
			$(".deleteNodeModal").modal()
			$(".deleteNodeModal").children(".hiddenInfo").val(id)
		else
			deleteNode(button)

manageInput = (flag) ->
	if flag 
		#go to first stage
		$(".levelName").attr('disabled', false)
		$(".nodeName").attr('disabled', false)
		$(".nodeText").attr('disabled', false)

		$('.linkButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.dndButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.deleteLinkButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.deleteDragButton').css({opacity: 1.0}).animate({opacity: 0}, 400)

		$('.delButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addNodeButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addFirstNode').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addLevelButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})

		setTimeout (->
			$("#doneButton").fadeIn()
			$('.linkButton').css({visibility: "hidden"})
			$('.dndButton').css({visibility: "hidden"})
			$('.deleteLinkButton').css({visibility: "hidden"})
			$('.deleteDragButton').css({visibility: "hidden"})
		), 400

		setTimeout (->
			$('.removeLevelButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.editButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			# to appear buttons after moving of elements to the down
		), 900
	else 
		#go to second stage
		$(".levelName").attr('disabled', true)
		$(".nodeName").attr('disabled', true)
		$(".nodeText").attr('disabled', true)
		$('.removeLevelButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.delButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addNodeButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addFirstNode').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addLevelButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.editButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		setTimeout (->
			$("#backButton").fadeIn()
			$("#startButton").fadeIn()
			$('.linkButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.dndButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.deleteLinkButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.deleteDragButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.removeLevelButton').css({visibility: "hidden"})
			$('.delButton').css({visibility: "hidden"})
			$('.addNodeButton').css({visibility: "hidden"})
			$('.addFirstNode').css({visibility: "hidden"})
			$('.addLevelButton').css({visibility: "hidden"})
			$('.editButton').css({visibility: "hidden"})
		), 400

createLine = (x1, y1, x2, y2, temp, from = "", to = "") ->
	if temp
		lineClass = "linetemp"
		color = "black"
	else
		lineClass = "line"
		colorInt = getRandomInt(0,4)
		switch colorInt
			when 0 then color = "red"
			when 1 then color = "blue"
			when 2 then color = "green"
			when 3 then color = "purple"
			when 4 then color = "orange"
	length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
	angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI
	transform = "rotate(" + angle + "deg)"
	if nodeToNumber < nodeFromNumber
		y1 = y2 # magical, fluke fix of mystical bug. 
	line = $("<div>").appendTo(".levelContanter").addClass(lineClass).attr('id', from+"y"+to).css(
		position: "relative"
		transform: transform
		background: color
	).width(length).offset(
		left: x1
		top: y1
	)
	return line


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                     linking functions                     #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

dragStart = (ev) ->
	# console.log ev
	id = ev.target.parentNode.parentNode.id
	linkButton = ev.target.className
	# console.log buttonClass
	nodeFromNumber = id.substring(6) # we need the last character(s) from string (example:) node1x2
	from = id.substring(4) # because node has 5 characters
	levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	width = $("#"+id).width()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x1 = left+width-92;
	y1 = top+height/2;


dragHover = (ev) ->
	ev.preventDefault()
	id = ev.target.parentNode.parentNode.id
	nodeToNumber = id.substring(6) # we need the last character(s) from string (example:) node1x2
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x2 = left+7
	y2 = top+(height)
	if levelTo > levelFrom && thinLine == 0
		ev.target.style.background = "#CCCCFF" #blue
		createLine(x1,y1,x2,y2,true)
		thinLine++

	if levelTo <= levelFrom
		ev.target.style.background = "#FFAAAA"

dragLeave = (ev) ->
	defaultBackground(ev)
	$(".linetemp").remove()
	thinLine = 0


deleteLink = (ev) ->
	ev.preventDefault()
	defaultBackground(ev)
	to = ev.target.parentNode.parentNode.id.substring(4) # because node has 4 characters
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	if levelTo > levelFrom
		selector = "#"+from+"y"+to
		# console.log selector
		if $(selector).length > 0 # exists
			el = array[from].indexOf(to) #find link in array
			array[from].splice(el, 1) # delete link from array

			if array[from].length == 0 # if last link deleted
				delete array[from]

			$(selector).remove()
		else
			writeError "this nodes are not linked"
	else
		writeError "deleting is allowed only to higher level node"
	$(".linetemp").remove()
	thinLine = 0


addLink = (ev) ->
	ev.preventDefault()
	defaultBackground(ev)
	to = ev.target.parentNode.parentNode.id.substring(4) # because node has 4 characters
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	if levelTo > levelFrom
		if array[from] == undefined
			array[from] = []  # create array for the first time

		if array[from].indexOf(to) > -1
			writeMessage "you do have this nodes linked"
		else
			array[from].push(to)
			array[from].sort()
			writeMessage "linked "+from+" to "+to
			createLine(x1,y1,x2,y2,false,from,to)
			# console.log array # debugging
		$(".linetemp").remove()
		thinLine = 0
	else
		writeError "can not link to current or previous level"

drop = (ev) ->
	if linkButton == "dndButton"
		addLink(ev)
	else if linkButton == "deleteDragButton"
		deleteLink(ev)
	else if linkButton == "linkButton"
		addLink(ev)
	else if linkButton == "deleteLinkButton"
		deleteLink(ev)

clickStart = (ev) ->
	id = ev.target.parentNode.parentNode.id
	nodeFromNumber = id.substring(6) # we need the last character(s) from string (example:) node1x2
	from = id.substring(4) # because node has 5 characters
	levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	width = $("#"+id).width()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x1 = left+width-92;
	y1 = top+height/2;
	linkButton = ev.target.className
	clickButtonActive = true


	
	
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                         algorythm                         #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

merge = (j) ->
	# adding linked node to path
	coolArray[a].push(toArray[j])
	# console.log "pushing "+toArray[j]+". now "+coolArray[a]+" in ["+a+"]"

	q = 0
	count = 0
	position = []
	check = toArray[j]

	# how many childrens has node
	while q < fromArray.length
		if check == fromArray[q]
			count++
			position.push(q)
		q++

	if count == 0 
		# the last node
		# console.log "this is the end, my only friend"
	else if count == 1 
		# the only one node
		merge(position[0])
	else
		# if more than one children
		k = 0
		while k < count
			# console.log "creating new chain"
			if k != count-1
				thisArray = coolArray[a].slice();
				coolArray[a+1] = coolArray[a].slice();
			else
				merge(position[k])
				wtf = true
				break

			merge(position[k])
			a++
			if wtf
				coolArray[a] = thisArray
				wtf = false
			k++


lala = (first,second) ->
	array = []
	if first.length == 0 && second.length == 0
		# nothing :) return empty array
	else if first.length == 0 && second.length > 0
		j = 0
		while j < second.length
			string = second[j]
			array.push(string)
			j++
	else if first.length > 0 && second.length == 0
		i = 0
		while i < first.length
			string = first[i]
			array.push(string)
			i++
	else
		i = 0
		while i < first.length
			j = 0
			while j < second.length
				string = first[i]+" "+second[j]
				array.push(string)
				j++
			i++
	return array




combine = (array) ->
	i = 0
	result = []
	while i < array.length
		temp = []
		j = 0
		while j < array[i].length-1
			if temp?.length == 0 # first node of path
				temp1 = $("#node"+array[i][j]).find(".nodeText").val().split(/\n/)
				arr1 = []
				k = 0
				while k < temp1.length
					arr1.push $.trim(temp1[k])  if /\S/.test(temp1[k])
					k++
			else 
				arr1 = temp
			

			temp2 = $("#node"+array[i][j+1]).find(".nodeText").val().split(/\n/)
			arr2 = []
			k = 0
			while k < temp2.length
				arr2.push $.trim(temp2[k])  if /\S/.test(temp2[k])
				k++
			

			temp = lala(arr1,arr2)
			# console.log temp

			j++
		i++
		result.push(temp)
	return result


start = ->
	size = Object.size(array)
	keys = Object.keys(array)
	
	firstNodes = []
	uniqueFirstNodes = []
	i = 0
	while i < size
		j = 0
		while j < array[keys[i]].length
			fromArray.push(keys[i])
			toArray.push(array[keys[i]][j])
			j++
		i++

	i = 0
	while i < fromArray.length
		unique = true
		j = 0
		while j < toArray.length
			if fromArray[i] == toArray[j]
				unique = false
				break
			j++
		if unique
			firstNodes.push(fromArray[i])
		i++
		
	$.each firstNodes, (i, el) ->
		uniqueFirstNodes.push el  if $.inArray(el, uniqueFirstNodes) is -1
	uniqueFirstNodes.sort()
	# console.log uniqueFirstNodes


	i = 0
	while i < uniqueFirstNodes.length
		j = 0
		while j < fromArray.length
			if uniqueFirstNodes[i] == fromArray[j]
				coolArray[a] = []
				coolArray[a].push(fromArray[j]) #add starting node
				# console.log "beginning: pushing "+fromArray[j]+". now "+coolArray[a]+" in ["+a+"]"
				merge(j)
				a++
			j++
		i++
	
	outputArrow = combine(coolArray)
	# console.log outputArrow

	i=0
	if $("#checkboxSortOutput").is ":checked"
		while i < outputArrow.length
			outputArrow[i].sort()
			i++

	i=0
	while i < outputArrow.length
		$(".outputContainer").append $(outputNodeCode).attr("id","output"+i)
		$("#output"+i).children("textarea").val(outputArrow[i].join("\n"))
		i++

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                     document handlers                     #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

$(document).on "click", "#doneButton", ->
	doneButtonClick()

$(document).on "click", "#backButton", ->
	backButtonClick()

$(document).on "click", "#startButton", ->
	start()

$(document).on "click", ".addNodeButton", ->
	addNodeButtonClick($(this))

$(document).on "click", ".addFirstNode", ->
	addFirstNodeButtonClick($(this))

$(document).on "click", ".delButton", ->
	delButtonClick($(this))

$(document).on "click", "#deleteNodeYes", ->
	id = $(".deleteNodeModal").children(".hiddenInfo").val()
	deleteNode(id,true)

$(document).on "click", "#deleteLevelYes", ->
	id = $(".deleteLevelModal").children(".hiddenInfo").val()
	deleteLevel(id,true)

$(document).on "click", ".removeLevelButton", ->
	removeLevelButtonClick($(this))

$(document).on "click", "#editModalDoneButton", ->
	parent = $(this).parent()
	newContent = $(parent).children("#editModalTextbox").val()
	$("#"+root.editingNode).children(".nodeCanvas").children("textarea").val(newContent)
	$('.editModal').modal('hide');

$(document).on "keydown", "#editModalTextbox", (e) ->
	if e.metaKey and e.keyCode is 13
		$("#editModalDoneButton").click()
	if e.ctrlKey and e.keyCode is 13
		$("#editModalDoneButton").click()
	# merging condition will cause bug: the first of listed key (ctrl / meta) will trigger click without enter key 

$(document).on "click", ".editButton", ->
	editButtonClick($(this))
	
$(document).on "click", "#settingsButton", ->
	$(".settingsModal").modal()

$(document).on "click", ".addLevelButton", ->
	addFirstNode(createLevel())
	deleteNodeButtons()
	addNodeButtons()
	deleteLevelButton()
	addLevelButton()
	writeId()
	tabIndexOrder()

$(document).ready ->
	addFirstNode(createLevel())
	addFirstNode(createLevel())
	writeId()
	tabIndexOrder()
	writeMessage("welcome to comgen2 beta v0.2.141126bbi")
	
$(window).load ->
	addNodeButtons()
	addLevelButton()

$(document).on "mouseenter", ".nodeCanvas", (event) ->
	if clickButtonActive
		dragHover(event)

$(document).on "mouseleave", ".nodeCanvas", (event) ->
	if clickButtonActive
		dragLeave(event)

$(document).keyup (e) ->
	if e.keyCode is 27 # esc
		clickButtonActive = false

$(document).on "click", ".nodeCanvas", (ev) ->
	if clickButtonActive
		clickButtonActive = false
		drop(ev)