var a, array, clickButtonActive, clickStart, combine, coolArray, createLine, defaultBackground, dragHover, dragLeave, dragStart, drop, firstClickLink, from, fromArray, getRandomInt, getTime, lala, levelClass, levelContainerClass, levelFrom, levelId, levelTo, manageInput, merge, nodeClass, nodeFromNumber, nodeToNumber, outputNodeCode, root, thinLine, to, toArray, writeError, writeMessage, wtf, x1, x2, y1, y2;

levelId = 1;

levelClass = "level";

levelContainerClass = "levelContanter";

nodeClass = "node";

array = [];

from = '';

to = '';

levelFrom = '';

levelTo = '';

nodeFromNumber = '';

nodeToNumber = '';

x1 = 0;

y1 = 0;

x2 = 0;

y2 = 0;

thinLine = 0;

fromArray = [];

toArray = [];

a = 0;

coolArray = [[]];

wtf = false;

clickButtonActive = false;

firstClickLink = '';

root = typeof exports !== "undefined" && exports !== null ? exports : this;

root.editingNode = '';

outputNodeCode = "<div class='outputNode' contentEditable> <textarea class='levelName'></textarea> </div>";

getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

getTime = function() {
  var hours, minutes, now, seconds, time;
  now = new Date();
  hours = now.getHours();
  minutes = now.getMinutes();
  seconds = now.getSeconds();
  return time = hours + ':' + minutes + ':' + seconds + ': ';
};

Object.size = function(obj) {
  var key, size;
  size = 0;
  key = void 0;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      size++;
    }
  }
  return size;
};

writeError = function(message) {
  return $(".console").prepend(getTime() + '<span style="color:red;">ERROR: </span>' + message + '<br>');
};

writeMessage = function(message) {
  return $(".console").prepend(getTime() + '<span style="color:#00AA00;">' + message + '</span><br>');
};

defaultBackground = function(ev) {
  return ev.target.style.background = "#EBEBE4";
};

createLine = function(x1, y1, x2, y2, temp) {
  var angle, color, colorInt, length, line, lineClass, transform;
  if (temp) {
    lineClass = "linetemp";
    color = "black";
  } else {
    lineClass = "line";
    colorInt = getRandomInt(0, 4);
    switch (colorInt) {
      case 0:
        color = "red";
        break;
      case 1:
        color = "blue";
        break;
      case 2:
        color = "green";
        break;
      case 3:
        color = "purple";
        break;
      case 4:
        color = "orange";
    }
  }
  length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
  angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  transform = "rotate(" + angle + "deg)";
  if (nodeToNumber < nodeFromNumber) {
    y1 = y2;
  }
  line = $("<div>").appendTo(".levelContanter").addClass(lineClass).css({
    position: "relative",
    transform: transform,
    background: color
  }).width(length).offset({
    left: x1,
    top: y1
  });
  return line;
};

dragStart = function(ev) {
  var height, id, left, offset, top, width;
  id = ev.target.parentNode.parentNode.id;
  nodeFromNumber = id.substring(6);
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  return y1 = top + height / 2;
};

dragHover = function(ev) {
  var height, id, left, offset, top;
  ev.preventDefault();
  id = ev.target.parentNode.parentNode.id;
  nodeToNumber = id.substring(6);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x2 = left + 7;
  y2 = top + height;
  if (levelTo > levelFrom && thinLine === 0) {
    ev.target.style.background = "#CCCCFF";
    createLine(x1, y1, x2, y2, true);
    thinLine++;
  }
  if (levelTo <= levelFrom) {
    return ev.target.style.background = "#FFAAAA";
  }
};

dragLeave = function(ev) {
  defaultBackground(ev);
  $(".linetemp").remove();
  return thinLine = 0;
};

drop = function(ev) {
  ev.preventDefault();
  defaultBackground(ev);
  to = ev.target.parentNode.parentNode.id.substring(4);
  levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  if (levelTo > levelFrom) {
    if (array[from] === void 0) {
      array[from] = [];
    }
    if (array[from].indexOf(to) > -1) {
      writeMessage("you do have this nodes linked");
    } else {
      array[from].push(to);
      array[from].sort();
      writeMessage("linked " + from + " to " + to);
      createLine(x1, y1, x2, y2, false);
    }
    $(".linetemp").remove();
    return thinLine = 0;
  } else {
    return writeError("can not link to current or previous level");
  }
};

manageInput = function(flag) {
  if (flag) {
    $(".levelName").attr('disabled', false);
    $(".nodeName").attr('disabled', false);
    $(".nodeText").attr('disabled', false);
    $('.linkButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.dndButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addNodeButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addFirstNode').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    $('.addLevelButton').css({
      opacity: 0.0,
      visibility: "visible"
    }).animate({
      opacity: 1.0
    });
    setTimeout((function() {
      $("#doneButton").fadeIn();
      $('.linkButton').css({
        visibility: "hidden"
      });
      return $('.dndButton').css({
        visibility: "hidden"
      });
    }), 400);
    return setTimeout((function() {
      $('.removeLevelButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      return $('.editButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
    }), 900);
  } else {
    $(".levelName").attr('disabled', true);
    $(".nodeName").attr('disabled', true);
    $(".nodeText").attr('disabled', true);
    $('.removeLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.delButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addNodeButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addFirstNode').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.addLevelButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    $('.editButton').css({
      opacity: 1.0
    }).animate({
      opacity: 0
    }, 400);
    return setTimeout((function() {
      $("#backButton").fadeIn();
      $("#startButton").fadeIn();
      $('.linkButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.dndButton').css({
        opacity: 0.0,
        visibility: "visible"
      }).animate({
        opacity: 1.0
      });
      $('.removeLevelButton').css({
        visibility: "hidden"
      });
      $('.delButton').css({
        visibility: "hidden"
      });
      $('.addNodeButton').css({
        visibility: "hidden"
      });
      $('.addFirstNode').css({
        visibility: "hidden"
      });
      $('.addLevelButton').css({
        visibility: "hidden"
      });
      return $('.editButton').css({
        visibility: "hidden"
      });
    }), 400);
  }
};

clickStart = function(ev) {
  var height, id, left, offset, top, width;
  if (!clickButtonActive) {
    clickButtonActive = true;
    ev.target.style.background = "green";
  } else {
    if (from !== ev.target.parentNode.parentNode.id.substring(4)) {
      $(".linkButton").css({
        background: "#0074d9"
      });
      ev.target.style.background = "green";
    } else {
      ev.target.style.background = "#0074d9";
      clickButtonActive = false;
    }
  }
  id = ev.target.parentNode.parentNode.id;
  firstClickLink;
  nodeFromNumber = id.substring(6);
  from = id.substring(4);
  levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5);
  offset = $("#" + id).offset();
  width = $("#" + id).width();
  height = $("#" + id).height();
  top = offset.top;
  left = offset.left;
  x1 = left + width - 92;
  return y1 = top + height / 2;
};

$(document).on("mouseenter", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    return dragHover(event);
  }
});

$(document).on("mouseleave", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    return dragLeave(event);
  }
});

$(document).on("click", ".nodeCanvas", function(event) {
  if (clickButtonActive) {
    drop(event);
    $(".linkButton").css({
      background: "#0074d9"
    });
    return clickButtonActive = false;
  }
});

$(document).keyup(function(e) {
  if (e.keyCode === 27) {
    clickButtonActive = false;
    return $(".linkButton").css({
      background: "#0074d9"
    });
  }
});

merge = function(j) {
  var check, count, k, position, q, thisArray, _results;
  coolArray[a].push(toArray[j]);
  q = 0;
  count = 0;
  position = [];
  check = toArray[j];
  while (q < fromArray.length) {
    if (check === fromArray[q]) {
      count++;
      position.push(q);
    }
    q++;
  }
  if (count === 0) {

  } else if (count === 1) {
    return merge(position[0]);
  } else {
    k = 0;
    _results = [];
    while (k < count) {
      if (k !== count - 1) {
        thisArray = coolArray[a].slice();
        coolArray[a + 1] = coolArray[a].slice();
      } else {
        merge(position[k]);
        wtf = true;
        break;
      }
      merge(position[k]);
      a++;
      if (wtf) {
        coolArray[a] = thisArray;
        wtf = false;
      }
      _results.push(k++);
    }
    return _results;
  }
};

lala = function(first, second) {
  var i, j, string;
  array = [];
  i = 0;
  while (i < first.length) {
    j = 0;
    while (j < second.length) {
      string = first[i] + " " + second[j];
      array.push(string);
      j++;
    }
    i++;
  }
  return array;
};

combine = function(array) {
  var arr1, arr2, i, j, k, result, temp, temp1, temp2;
  i = 0;
  result = [];
  while (i < array.length) {
    temp = [];
    j = 0;
    while (j < array[i].length - 1) {
      if ((temp != null ? temp.length : void 0) === 0) {
        temp1 = $("#node" + array[i][j]).find(".nodeText").val().split(/\n/);
        arr1 = [];
        k = 0;
        while (k < temp1.length) {
          if (/\S/.test(temp1[k])) {
            arr1.push($.trim(temp1[k]));
          }
          k++;
        }
      } else {
        arr1 = temp;
      }
      temp2 = $("#node" + array[i][j + 1]).find(".nodeText").val().split(/\n/);
      arr2 = [];
      k = 0;
      while (k < temp2.length) {
        if (/\S/.test(temp2[k])) {
          arr2.push($.trim(temp2[k]));
        }
        k++;
      }
      temp = lala(arr1, arr2);
      j++;
    }
    i++;
    result.push(temp);
  }
  return result;
};

$(document).on("click", "#doneButton", function() {
  $("#doneButton").fadeOut();
  manageInput(false);
  $(".nodeName").animate({
    "left": "-=140px"
  });
  $(".nodeName").animate({
    "top": "-=125px"
  });
  $(".levelName").animate({
    "top": "-=20px"
  });
  $(".nodeText").css({
    background: "#EBEBE4"
  });
  $(".nodeName").css({
    background: "#EBEBE4"
  });
  return $(".levelName").css({
    background: "#EBEBE4"
  });
});

$(document).on("click", "#backButton", function() {
  var i, keys, size, _results;
  $("#backButton").fadeOut();
  $("#startButton").fadeOut();
  manageInput(true);
  $(".line").remove();
  $(".linetemp").remove();
  $(".nodeName").animate({
    "left": "+=140px"
  });
  $(".nodeName").animate({
    "top": "+=125px"
  });
  setTimeout((function() {
    return $(".levelName").animate({
      "top": "+=20px"
    });
  }), 600);
  $(".nodeText").css({
    background: "#FFF"
  });
  $(".nodeName").css({
    background: "#FFF"
  });
  $(".levelName").css({
    background: "#FFF"
  });
  thinLine = 0;
  fromArray = [];
  toArray = [];
  a = 0;
  coolArray = [[]];
  wtf = false;
  i = 0;
  size = Object.size(array);
  keys = Object.keys(array);
  _results = [];
  while (i < size) {
    delete array[keys[i]];
    _results.push(i++);
  }
  return _results;
});

$(document).on("click", "#startButton", function() {
  var firstNodes, i, j, keys, outputArrow, size, unique, uniqueFirstNodes, _results;
  size = Object.size(array);
  keys = Object.keys(array);
  firstNodes = [];
  uniqueFirstNodes = [];
  i = 0;
  while (i < size) {
    j = 0;
    while (j < array[keys[i]].length) {
      fromArray.push(keys[i]);
      toArray.push(array[keys[i]][j]);
      j++;
    }
    i++;
  }
  i = 0;
  while (i < fromArray.length) {
    unique = true;
    j = 0;
    while (j < toArray.length) {
      if (fromArray[i] === toArray[j]) {
        unique = false;
        break;
      }
      j++;
    }
    if (unique) {
      firstNodes.push(fromArray[i]);
    }
    i++;
  }
  $.each(firstNodes, function(i, el) {
    if ($.inArray(el, uniqueFirstNodes) === -1) {
      return uniqueFirstNodes.push(el);
    }
  });
  uniqueFirstNodes.sort();
  i = 0;
  while (i < uniqueFirstNodes.length) {
    j = 0;
    while (j < fromArray.length) {
      if (uniqueFirstNodes[i] === fromArray[j]) {
        coolArray[a] = [];
        coolArray[a].push(fromArray[j]);
        merge(j);
        a++;
      }
      j++;
    }
    i++;
  }
  outputArrow = combine(coolArray);
  i = 0;
  if ($("#checkboxSortOutput").is(":checked")) {
    while (i < outputArrow.length) {
      outputArrow[i].sort();
      i++;
    }
  }
  i = 0;
  _results = [];
  while (i < outputArrow.length) {
    $(".outputContainer").append($(outputNodeCode).attr("id", "output" + i));
    $("#output" + i).children("textarea").val(outputArrow[i].join("\n"));
    _results.push(i++);
  }
  return _results;
});
