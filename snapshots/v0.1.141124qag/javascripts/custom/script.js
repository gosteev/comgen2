(function() {
  var addFirstNode, addFirstNodeCode, addLevelButton, addLevelButtonCode, addNodeButtonCode, addNodeButtons, createLevel, deleteId, deleteLevel, deleteLevelButton, deleteNode, deleteNodeButtons, getParentId, getParentParentId, getParentParentParentId, levelClass, levelCode, levelContainerClass, levelId, nodeClass, nodeCode, parseId, sortLevelId, sortNodeId, writeId;

  levelId = 1;

  levelClass = "level";

  levelContainerClass = "levelContanter";

  nodeClass = "node";

  levelCode = "<div> <div class='levelButtonsWrapper'> <div class='removeLevelButtonWrapper'> <button class='removeLevelButton'>remove level</button> </div> <div class='levelText'> <input class='levelName' placeholder='level name'> </div> </div> </div>";

  nodeCode = "<div> <div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'> <textarea class='nodeText'></textarea> </div> <div class='buttons'> <button class='linkButton' onclick='clickStart(event)'>click link</button> <button class='dndButton' draggable='true' ondragstart='dragStart(event)'>drag link</button> <button class='editButton'>edit</button> <input class='nodeName' placeholder='node name'> <div class='buttonBr'></div> <button class='delButton'>delele</button> </div> </div>";

  addNodeButtonCode = "<div class='addNodeButtonWrapper'> <button class='addNodeButton'>add node</button> </div>";

  addLevelButtonCode = "<div class='addLevelButtonWrapper'> <button class='addLevelButton'>add level</button> </div>";

  addFirstNodeCode = "<div class='" + nodeClass + " nodeEmpty'> <div class='addNodeButtonWrapper'> <button class='addFirstNode'>add node</button> </div> </div>";

  getParentParentParentId = function(item) {
    return item.parent().parent().parent().attr('id');
  };

  getParentParentId = function(item) {
    return item.parent().parent().attr('id');
  };

  getParentId = function(item) {
    return item.parent().attr('id');
  };

  createLevel = function() {
    $("." + levelContainerClass).append($(levelCode).attr("id", levelClass + levelId).attr("class", levelClass));
    return levelId++;
  };

  parseId = function(id) {
    var array, foo;
    foo = id.substring(4);
    array = foo.split("x");
    return array;
  };

  sortNodeId = function(id) {
    var array;
    array = parseId(id);
    return $("#" + levelClass + array[0] + " ." + nodeClass).each(function(i) {
      return $(this).attr('id', nodeClass + array[0] + 'x' + (i + 1));
    });
  };

  sortLevelId = function() {
    return $("." + levelClass).each(function(i) {
      $(this).attr('id', levelClass + (i + 1));
      return $(this).children("." + nodeClass).each(function(j) {
        return $(this).attr('id', nodeClass + (i + 1) + 'x' + (j + 1));
      });
    });
  };

  addNodeButtons = function() {
    return $("." + levelClass + " ." + nodeClass + ":last-child").each(function(i) {
      if (!($(this).hasClass("nodeEmpty"))) {
        return $(this).append(addNodeButtonCode);
      }
    });
  };

  deleteNodeButtons = function() {
    return $(".addNodeButtonWrapper").each(function(i) {
      if (!($(this).parent().hasClass("nodeEmpty"))) {
        return $(this).remove();
      }
    });
  };

  addLevelButton = function() {
    return $("." + levelClass + ":last-child .levelButtonsWrapper").append(addLevelButtonCode);
  };

  deleteLevelButton = function() {
    return $(".addLevelButtonWrapper").remove();
  };

  addFirstNode = function(level) {
    var nodeId;
    nodeId = level + "x1";
    return $("#" + levelClass + level).append($(nodeCode).attr("id", nodeClass + nodeId).attr("class", nodeClass));
  };

  deleteId = function() {
    return $(".matrixPosition").remove();
  };

  writeId = function(node) {
    deleteId();
    return $("." + nodeClass).each(function(i) {
      var id;
      id = $(this).attr('id').substring(4);
      return $(this).children(".buttons").append("<span class='matrixPosition'>" + id + "</span>");
    });
  };

  $(document).ready(function() {
    addFirstNode(createLevel());
    addFirstNode(createLevel());
    return writeId();
  });

  $(document).on("click", ".addNodeButton", function() {
    var array;
    array = parseId(getParentParentId($(this)));
    array[1]++;
    $("#level" + array[0]).append($(nodeCode).attr("id", nodeClass + array[0] + 'x' + array[1]).attr("class", nodeClass));
    deleteNodeButtons();
    addNodeButtons();
    return writeId();
  });

  $(document).on("click", ".addFirstNode", function() {
    var bar, id;
    id = getParentParentParentId($(this));
    bar = id.substring(5);
    $("#" + levelClass + bar).append($(nodeCode).attr("id", nodeClass + bar + "x1").attr("class", nodeClass));
    $("#" + levelClass + bar + " .nodeEmpty").remove();
    deleteNodeButtons();
    addNodeButtons();
    return writeId();
  });

  deleteNode = function(node, a) {
    var array, id, nextNode;
    if (a) {
      id = node;
    } else {
      id = getParentParentId(node);
    }
    $("#" + id).remove();
    array = parseId(id);
    nextNode = parseInt(array[1]);
    nextNode++;
    if (array[1] === "1" && $('#' + nodeClass + array[0] + 'x' + nextNode).length === 0) {
      return $("#" + levelClass + array[0]).append(addFirstNodeCode);
    } else {
      sortNodeId(id);
      deleteNodeButtons();
      addNodeButtons();
      return writeId();
    }
  };

  $(document).on("click", ".delButton", function() {
    var content, id;
    if ($("#checkboxConfirmation").is(":checked")) {
      return deleteNode($(this));
    } else {
      id = getParentParentId($(this));
      content = $("#" + id).children(".nodeCanvas").children("textarea").val();
      if (content.length > 0) {
        $(".deleteNodeModal").modal();
        return $(".deleteNodeModal").children(".hiddenInfo").val(id);
      } else {
        return deleteNode($(this));
      }
    }
  });

  $(document).on("click", "#deleteNodeYes", function() {
    var id;
    id = $(".deleteNodeModal").children(".hiddenInfo").val();
    return deleteNode(id, true);
  });

  $(document).on("click", "#deleteLevelYes", function() {
    var id;
    id = $(".deleteLevelModal").children(".hiddenInfo").val();
    return deleteLevel(id, true);
  });

  deleteLevel = function(level, a) {
    var bar, id;
    if (a) {
      id = level;
    } else {
      id = getParentParentParentId($(level));
    }
    bar = id.substring(5);
    if (bar === "1" && $('#' + levelClass + "2").length === 0) {
      return $(".lastLevelModal").modal();
    } else {
      $("#" + id).remove();
      levelId--;
      sortLevelId();
      deleteLevelButton();
      addLevelButton();
      return writeId();
    }
  };

  $(document).on("click", ".removeLevelButton", function() {
    var id;
    if ($("#checkboxConfirmation").is(":checked")) {
      return deleteLevel($(this));
    } else {
      id = getParentParentParentId($(this));
      $(".deleteLevelModal").modal();
      return $(".deleteLevelModal").children(".hiddenInfo").val(id);
    }
  });

  $(document).on("click", "#editModalDoneButton", function() {
    var newContent, parent;
    parent = $(this).parent();
    newContent = $(parent).children("#editModalTextbox").val();
    $("#" + root.editingNode).children(".nodeCanvas").children("textarea").val(newContent);
    return $('.editModal').modal('hide');
  });

  $(document).on("click", ".editButton", function() {
    var id, nodeContent;
    id = getParentParentId($(this));
    root.editingNode = id;
    nodeContent = $("#" + id).children(".nodeCanvas").children("textarea").val();
    $(".editModal").modal();
    return $("#editModalTextbox").val(nodeContent);
  });

  $(document).on("click", "#settingsButton", function() {
    return $(".settingsModal").modal();
  });

  $(document).on("click", ".addLevelButton", function() {
    addFirstNode(createLevel());
    deleteNodeButtons();
    addNodeButtons();
    deleteLevelButton();
    addLevelButton();
    return writeId();
  });

  $(window).load(function() {
    addNodeButtons();
    return addLevelButton();
  });

}).call(this);
