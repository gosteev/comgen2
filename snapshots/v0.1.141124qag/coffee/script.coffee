levelId = 1;
levelClass = "level"
levelContainerClass = "levelContanter"
nodeClass = "node"

levelCode = "
<div>
	<div class='levelButtonsWrapper'>
		<div class='removeLevelButtonWrapper'>
		<button class='removeLevelButton'>remove level</button>
	</div>
	<div class='levelText'>
		<input class='levelName' placeholder='level name'>
	</div>
</div>
</div>
"

nodeCode = "
<div>
	<div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'>
		<textarea class='nodeText'></textarea>
	</div>
	<div class='buttons'>
		<button class='linkButton' onclick='clickStart(event)'>click link</button>
		<button class='dndButton' draggable='true' ondragstart='dragStart(event)'>drag link</button>
		<button class='editButton'>edit</button>
		<input class='nodeName' placeholder='node name'>
		<div class='buttonBr'></div>
		<button class='delButton'>delele</button>
	</div>
</div>
"

addNodeButtonCode = "
<div class='addNodeButtonWrapper'>
	<button class='addNodeButton'>add node</button>
</div>
"

addLevelButtonCode = "
<div class='addLevelButtonWrapper'>
	<button class='addLevelButton'>add level</button>
</div>
"

addFirstNodeCode = "
<div class='"+nodeClass+" nodeEmpty'>
<div class='addNodeButtonWrapper'>
	<button class='addFirstNode'>add node</button>
</div>
</div>
"

getParentParentParentId = (item) ->
	item.parent().parent().parent().attr('id');

getParentParentId = (item) ->
	item.parent().parent().attr('id');

getParentId = (item) ->
	item.parent().attr('id');

createLevel = ->
	$("."+levelContainerClass).append $(levelCode).attr("id",levelClass+levelId).attr("class",levelClass)
	levelId++

parseId = (id) ->
	foo = id.substring(4) # because node has 4 characters
	array = foo.split("x");
	return array

sortNodeId = (id) ->
	array = parseId(id)
	$("#"+levelClass+array[0]+" ."+nodeClass).each (i) ->
		$(this).attr('id', nodeClass+array[0]+'x'+(i+1));

sortLevelId = ->
	$("."+levelClass).each (i) ->
		$(this).attr('id', levelClass+(i+1));
		$(this).children("."+nodeClass).each (j) ->
			$(this).attr('id', nodeClass+(i+1)+'x'+(j+1))

addNodeButtons = ->
	$("."+levelClass+" ."+nodeClass+":last-child").each (i) ->
		if !($(this).hasClass("nodeEmpty"))
			$(this).append(addNodeButtonCode)

deleteNodeButtons = ->
	$(".addNodeButtonWrapper").each (i) ->
		if !($(this).parent().hasClass("nodeEmpty"))
			$(this).remove()

addLevelButton = ->
	$("."+levelClass+":last-child .levelButtonsWrapper").append(addLevelButtonCode)

deleteLevelButton = ->
	$(".addLevelButtonWrapper").remove()

addFirstNode = (level) ->
	nodeId = level + "x1"
	$("#"+levelClass+level).append $(nodeCode).attr("id",nodeClass+nodeId).attr("class",nodeClass)

deleteId = ->
	$(".matrixPosition").remove()

writeId = (node) ->
	deleteId()
	$("."+nodeClass).each (i) ->
		id = $(this).attr('id').substring(4)
		$(this).children(".buttons").append "<span class='matrixPosition'>" + id + "</span>"




$(document).ready ->
	addFirstNode(createLevel())
	addFirstNode(createLevel())
	writeId()
	
$(document).on "click", ".addNodeButton", ->
	array = parseId(getParentParentId($(this)))

	array[1]++
	$("#level"+array[0]).append $(nodeCode).attr("id",nodeClass+array[0]+'x'+array[1]).attr("class",nodeClass)

	deleteNodeButtons()
	addNodeButtons()
	
	writeId()

$(document).on "click", ".addFirstNode", ->
	id = getParentParentParentId($(this))
	bar = id.substring(5) # because level has 5 characters
	$("#"+levelClass+bar).append $(nodeCode).attr("id",nodeClass+bar+"x1").attr("class",nodeClass)
	$("#"+levelClass+bar+" .nodeEmpty").remove()

	deleteNodeButtons()
	addNodeButtons()
	writeId()


deleteNode = (node,a) ->
	if a
		id = node
	else
		id = getParentParentId(node)
	$("#"+id).remove()
	array = parseId(id)
	nextNode = parseInt(array[1])
	nextNode++
	if array[1] == "1" && $('#'+nodeClass+array[0]+'x'+nextNode).length == 0
		$("#"+levelClass+array[0]).append(addFirstNodeCode);
	else
		sortNodeId(id)
		deleteNodeButtons()
		addNodeButtons()
		writeId()

$(document).on "click", ".delButton", ->
	if $("#checkboxConfirmation").is ":checked"
		deleteNode($(this))
	else
		id = getParentParentId($(this))
		content = $("#"+id).children(".nodeCanvas").children("textarea").val()

		if content.length > 0
			$(".deleteNodeModal").modal()
			$(".deleteNodeModal").children(".hiddenInfo").val(id)
		else
			deleteNode($(this))


$(document).on "click", "#deleteNodeYes", ->
	id = $(".deleteNodeModal").children(".hiddenInfo").val()
	deleteNode(id,true)

$(document).on "click", "#deleteLevelYes", ->
	id = $(".deleteLevelModal").children(".hiddenInfo").val()
	deleteLevel(id,true)


deleteLevel = (level,a) ->
	if a
		id = level
	else
		id = getParentParentParentId($(level))

	bar = id.substring(5) # because level has 5 characters
	if bar == "1" && $('#'+levelClass+"2").length == 0
		$(".lastLevelModal").modal()
	else
		$("#"+id).remove()
		levelId--
		sortLevelId()
		deleteLevelButton()
		addLevelButton()
		writeId()

$(document).on "click", ".removeLevelButton", ->
	if $("#checkboxConfirmation").is ":checked"
		deleteLevel($(this))
	else
		id = getParentParentParentId($(this))
		$(".deleteLevelModal").modal()
		$(".deleteLevelModal").children(".hiddenInfo").val(id)


$(document).on "click", "#editModalDoneButton", ->
	parent = $(this).parent()
	newContent = $(parent).children("#editModalTextbox").val()
	$("#"+root.editingNode).children(".nodeCanvas").children("textarea").val(newContent)
	$('.editModal').modal('hide');



$(document).on "click", ".editButton", ->
	id = getParentParentId($(this))
	root.editingNode = id
	nodeContent = $("#"+id).children(".nodeCanvas").children("textarea").val()
	$(".editModal").modal()
	$("#editModalTextbox").val(nodeContent)

$(document).on "click", "#settingsButton", ->
	$(".settingsModal").modal()

# $(document).on "click", "#settingsCancelButton", ->
# 	$(".settingsConfirmModal").modal()

# $(document).on "click", "#settingsConfirmNoButton", ->
# 	$(".settingsModal").modal()

$(document).on "click", ".addLevelButton", ->
	addFirstNode(createLevel())
	deleteNodeButtons()
	addNodeButtons()
	deleteLevelButton()
	addLevelButton()
	writeId()


$(window).load ->
	addNodeButtons()
	addLevelButton()