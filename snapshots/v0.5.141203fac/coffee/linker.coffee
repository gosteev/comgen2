# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ##################################################################################################
#                     global variables                      ##################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ##################################################################################################

levelId = 1;
levelClass = "level"
levelContainerClass = "levelContanter"
nodeClass = "node"
array = []
from = ''
fromL = ''
to = ''
levelFrom = ''
levelTo = ''
nodeFromNumber = ''
nodeToNumber = ''
x1 = 0
y1 = 0
x2 = 0
y2 = 0
thinLine = 0
fromArray = []
toArray = []
a = 0
coolArray = [[]]
wtf = false
clickButtonActive = false
linkButton = ''
firstClickLink = ''
widthSlider = []

# very globar variables :)
root = exports ? this
root.editingNode = ''

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                            texts                          ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

outputNodeCode = "
<div class='outputNode'>
	<div class='outputNodeName'></div>
	<textarea class='outputNodeContent'></textarea>
</div>
"
levelCode = "
<div>
	<div class='levelButtonsWrapper'>
		<div class='removeLevelButtonWrapper'>
		<button class='removeLevelButton'>remove level</button>
	</div>
	<div class='levelText'>
		<input class='levelName' placeholder='level name'>
	</div>
	<div class='levelDragArea' ondrop='dropLevel(event)' ondragover='dragHoverLevel(event)' ondragleave='dragLeaveLevel(event)'></div>
	<button class='levelDragButton' draggable='true' ondragstart='dragStartLevel(event)'>l.drag</button>
</div>
</div>
"
nodeCode = "
<div>
	<div class='nodeCanvas' ondrop='drop(event)' ondragover='dragHover(event)' ondragleave='dragLeave(event)'>
		<textarea class='nodeText'></textarea>
	</div>
	<div class='buttons'>
		<button class='linkButton' onclick='clickStart(event)'>click</button>
		<button class='dndButton' draggable='true' ondragstart='dragStart(event)'>drag</button>
		<button class='deleteLinkButton' onclick='clickStart(event)'>click delete</button>
		<button class='deleteDragButton' draggable='true' ondragstart='dragStart(event)'>drag delete</button>
		<button class='editButton'>edit</button>
		<input class='nodeName' placeholder='node name'>
		<div class='buttonBr'></div>
		<button class='delButton'>delete</button>
	</div>
</div>
"
addNodeButtonCode = "
<div class='addNodeButtonWrapper'>
	<button class='addNodeButton'>add node</button>
</div>
"
addLevelButtonCode = "
<div class='addLevelButtonWrapper'>
	<button class='addLevelButton'>add level</button>
</div>
"
addFirstNodeCode = "
<div class='"+nodeClass+" nodeEmpty'>
<div class='addNodeButtonWrapper'>
	<button class='addFirstNode'>add node</button>
</div>
</div>
"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                     helper functions                      ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

uppercaseFirstLetter = (string) ->
	string.charAt(0).toUpperCase() + string.slice(1)

lowercaseFirstLetter = (string) ->
	string.charAt(0).toLowerCase() + string.slice(1)

uppercaseFirstLetterAllWords = (str) ->
	str.replace /\w\S*/g, (txt) ->
		txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()

lowercaseFirstLetterAllWords = (str) ->
	str.replace /\w\S*/g, (txt) ->
		txt.charAt(0).toLowerCase() + txt.substr(1).toUpperCase()

getRandomInt = (min, max) ->
	Math.floor(Math.random() * (max - min + 1)) + min

getTime = (flag) ->
	now = new Date()
	hours = now.getHours()
	minutes = now.getMinutes()
	seconds = now.getSeconds()
	if flag
		time = hours+':'+minutes+':'+seconds+': '
	else
		now

Object.size = (obj) ->
	size = 0
	key = undefined
	for key of obj
		size++  if obj.hasOwnProperty(key)
	return size

getParentParentParentId = (item) ->
	item.parent().parent().parent().attr('id');

getParentParentId = (item) ->
	item.parent().parent().attr('id');

getParentId = (item) ->
	item.parent().attr('id');

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                      layout functions                     ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

addFirstNode = (level) ->
	nodeId = level + "x1"
	$("#"+levelClass+level).append $(nodeCode).attr("id",nodeClass+nodeId).attr("class",nodeClass)

createLevel = ->
	$("."+levelContainerClass).append $(levelCode).attr("id",levelClass+levelId).attr("class",levelClass)
	levelId++

parseId = (id) ->
	foo = id.substring(4) # because node has 4 characters
	arrayR = foo.split("x");
	return arrayR

sortNodeId = (id) ->
	arrayR = parseId(id)
	$("#"+levelClass+arrayR[0]+" ."+nodeClass).each (i) ->
		$(this).attr('id', nodeClass+arrayR[0]+'x'+(i+1));

sortLevelId = ->
	$("."+levelClass).each (i) ->
		$(this).attr('id', levelClass+(i+1));
		$(this).children("."+nodeClass).each (j) ->
			$(this).attr('id', nodeClass+(i+1)+'x'+(j+1))

addNodeButtons = ->
	$("."+levelClass+" ."+nodeClass+":last-child").each (i) ->
		if !($(this).hasClass("nodeEmpty"))
			$(this).append(addNodeButtonCode)

deleteNodeButtons = ->
	$(".addNodeButtonWrapper").each (i) ->
		if !($(this).parent().hasClass("nodeEmpty"))
			$(this).remove()

addLevelButton = ->
	$("."+levelClass+":last-child .levelButtonsWrapper").append(addLevelButtonCode)

deleteLevelButton = ->
	$(".addLevelButtonWrapper").remove()

deleteId = ->
	$(".matrixPosition").remove()

writeId = (node) ->
	deleteId()
	$("."+nodeClass).each (i) ->
		id = $(this).attr('id').substring(4)
		$(this).children(".buttons").append "<span class='matrixPosition'>" + id + "</span>"

tabIndexOrder = ->
	i = 1
	$(".levelName").each ->
		$(this).attr "tabindex", i
		i++
	$(".nodeName").each ->
		$(this).attr "tabindex", i
		i++
	$(".nodeText").each ->
		$(this).attr "tabindex", i
		i++

writeError = (message) ->
	$(".console").prepend(getTime(true)+'<span style="color:red;">ERROR: </span>'+message+'<br>');

writeMessage = (message) ->
	$(".console").prepend(getTime(true)+'<span style="color:#00AA00;">'+message+'</span><br>');

defaultBackground = (ev) ->
	ev.target.style.background = "#EBEBE4"

deleteNode = (node,a) ->
	if a
		id = node
	else
		id = getParentParentId(node)
	$("#"+id).remove()
	arrayR = parseId(id)
	nextNode = parseInt(arrayR[1])
	nextNode++
	if arrayR[1] == "1" && $('#'+nodeClass+arrayR[0]+'x'+nextNode).length == 0
		$("#"+levelClass+arrayR[0]).append(addFirstNodeCode);
	else
		sortNodeId(id)
		deleteNodeButtons()
		addNodeButtons()
		writeId()
		tabIndexOrder()

deleteLevel = (level,a) ->
	if a
		id = level
	else
		id = getParentParentParentId($(level))

	bar = id.substring(5) # because level has 5 characters
	if bar == "1" && $('#'+levelClass+"2").length == 0
		$(".lastLevelModal").modal()
	else
		$("#"+id).remove()
		levelId--
		sortLevelId()
		deleteLevelButton()
		addLevelButton()
		writeId()
		tabIndexOrder()

doneButtonClick = ->
	$("#doneButton").fadeOut()
	manageInput(false)
	$(".nodeName").animate({"left":"-=140px" });
	$(".nodeName").animate({"top":"-=27px" });
	$(".levelName").animate({"top":"-=20px" });
	$(".nodeText").css({background: "#EBEBE4"})
	$(".nodeName").css({background: "#EBEBE4"})
	$(".levelName").css({background: "#EBEBE4"})

backButtonClick = ->
	$("#backButton").fadeOut()
	$("#startButton").fadeOut()
	$("#csvExportButton").fadeOut()
	manageInput(true)
	$(".line").remove()
	$(".linetemp").remove()
	$(".nodeName").animate({"left":"+=140px" })
	$(".nodeName").animate({"top":"+=27px" })
	setTimeout (->
			$(".levelName").animate({"top":"+=20px" })
	), 600
	$(".nodeText").css({background: "#FFF", "border-color": "#777"})
	$(".nodeName").css({background: "#FFF"})
	$(".levelName").css({background: "#FFF"})

	$(".nodeText").css

	# null global variables
	array = []
	from = ''
	to = ''
	levelFrom = ''
	levelTo = ''
	nodeFromNumber = ''
	nodeToNumber = ''
	x1 = 0
	y1 = 0
	x2 = 0
	y2 = 0
	thinLine = 0
	fromArray = []
	toArray = []
	a = 0
	coolArray = [[]]
	wtf = false
	clickButtonActive = false
	linkButton = ''
	firstClickLink = ''
	widthSlider = []


	# clearing array of links
	i = 0
	size = Object.size(array)
	keys = Object.keys(array)
	while i < size
		delete array[keys[i]]
		i++

	if $(".outputContainer").length
		$(".outputContainer").remove()

	writeMessage("-=SCRATCH=-")

removeLevelButtonClick = (button) ->
	if $("#checkboxConfirmation").is ":checked"
		deleteLevel(button)
	else
		id = getParentParentParentId(button)
		$(".deleteLevelModal").modal()
		$(".deleteLevelModal").children(".hiddenInfo").val(id)

editButtonClick = (button) ->
	id = getParentParentId(button)
	root.editingNode = id
	nodeContent = $("#"+id).children(".nodeCanvas").children("textarea").val()
	$(".editModal").modal()
	$("#editModalTextbox").val(nodeContent)

addFirstNodeButtonClick = (button) ->
	id = getParentParentParentId(button)
	bar = id.substring(5) # because level has 5 characters
	$("#"+levelClass+bar).append $(nodeCode).attr("id",nodeClass+bar+"x1").attr("class",nodeClass)
	$("#"+levelClass+bar+" .nodeEmpty").remove()
	deleteNodeButtons()
	addNodeButtons()
	writeId()
	tabIndexOrder()

addNodeButtonClick = (button) ->
	arrayR = parseId(getParentParentId(button))
	arrayR[1]++
	$("#level"+arrayR[0]).append $(nodeCode).attr("id",nodeClass+arrayR[0]+'x'+arrayR[1]).attr("class",nodeClass)
	deleteNodeButtons()
	addNodeButtons()
	writeId()
	tabIndexOrder()

delButtonClick = (button) ->
	if $("#checkboxConfirmation").is ":checked"
		deleteNode(button)
	else
		id = getParentParentId(button)
		content = $("#"+id).children(".nodeCanvas").children("textarea").val()

		if content.length > 0
			$(".deleteNodeModal").modal()
			$(".deleteNodeModal").children(".hiddenInfo").val(id)
		else
			deleteNode(button)

manageInput = (flag) ->
	if flag 
		#go to first stage
		$(".levelName").attr('disabled', false)
		$(".nodeName").attr('disabled', false)
		$(".nodeText").attr('disabled', false)

		$('.linkButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.dndButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.deleteLinkButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.deleteDragButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.levelDragArea').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.levelDragButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('#outputSlider').css({opacity: 1.0}).animate({opacity: 0}, 400)

		$('.delButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addNodeButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addFirstNode').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
		$('.addLevelButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})

		setTimeout (->
			$("#doneButton").fadeIn()
			$('.linkButton').css({visibility: "hidden"})
			$('.dndButton').css({visibility: "hidden"})
			$('.deleteLinkButton').css({visibility: "hidden"})
			$('.deleteDragButton').css({visibility: "hidden"})
			$('.levelDragArea').css({visibility: "hidden"})
			$('.levelDragButton').css({visibility: "hidden"})
			$('#outputSlider').css({visibility: "hidden"})
		), 400

		setTimeout (->
			$('.removeLevelButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.editButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			# to appear buttons after moving of elements to the down
		), 900

	else 
		#go to second stage
		$(".levelName").attr('disabled', true)
		$(".nodeName").attr('disabled', true)
		$(".nodeText").attr('disabled', true)

		$('.removeLevelButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.delButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addNodeButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addFirstNode').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.addLevelButton').css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.editButton').css({opacity: 1.0}).animate({opacity: 0}, 400)

		$('.levelDragArea').first().css({opacity: 1.0}).animate({opacity: 0}, 400)
		$('.levelDragButton').last().css({opacity: 1.0}).animate({opacity: 0}, 400)

		setTimeout (->
			$("#backButton").fadeIn()
			$("#startButton").fadeIn()

			$('.linkButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.dndButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.deleteLinkButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.deleteDragButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.levelDragArea').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
			$('.levelDragButton').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})

			$('.removeLevelButton').css({visibility: "hidden"})
			$('.delButton').css({visibility: "hidden"})
			$('.addNodeButton').css({visibility: "hidden"})
			$('.addFirstNode').css({visibility: "hidden"})
			$('.addLevelButton').css({visibility: "hidden"})
			$('.editButton').css({visibility: "hidden"})

			$('.levelDragArea').first().css({visibility: "hidden"})
			$('.levelDragButton').last().css({visibility: "hidden"})
		), 400

createLine = (x1, y1, x2, y2, temp, from = "", to = "") ->
	if temp
		lineClass = "linetemp"
		color = "black"
	else
		lineClass = "line"
		colorInt = getRandomInt(0,4)
		switch colorInt
			when 0 then color = "red"
			when 1 then color = "blue"
			when 2 then color = "green"
			when 3 then color = "purple"
			when 4 then color = "orange"
	y1 = y1 - $(".levelContanter").scrollTop()
	x1 = x1 - $(".levelContanter").scrollLeft()

	# console.log "x1: "+x1
	# console.log "y1: "+y1
	# console.log "x2: "+x2
	# console.log "y2: "+y2


	length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
	angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI


	transform = "rotate(" + angle + "deg)"

	# yTemp = y1
	# if nodeToNumber < nodeFromNumber and nodeFromNumber < 10 
	# 	y1 = y2 # magical, fluke fix of mystical bug.
	# if nodeFromNumber >=2 and nodeToNumber >=10
	# 	y1 = yTemp # another wtf bug fix
	# if nodeFromNumber >=10
	# 	y1 = y2
	# 	console.log y1
	# 	console.log y2
	# 	console.log yTemp


	line = $("<div>").appendTo(".levelContanter").addClass(lineClass).attr('id', from+"y"+to).css(
		position: "absolute"
		transform: transform
		background: color
	).width(length).offset(
		left: x1
		top: y1 # FIX THIS
	)
	return line


fixLines = ->
	$(".line").each ->
		ida = $(this).attr('id').split("y")
		hh = $("#node"+ida[0]).offset().top
		hh += 100
		$(this).css("top", hh)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                     linking functions                     ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

dragStartLevel = (ev) ->
	fromL = ev.target.parentNode.parentNode.id

dropLevel = (ev) ->
	ev.target.style.background = "#aaa"
	l2 = ev.target.parentNode.parentNode.id
	if fromL.charAt(0) == "l" # level -> level
		$("#"+fromL+" .node").each () ->
			id = $(this).context.id
			foo = parseId(id)
			nodeFromNumber = foo[1]
			# console.log "from: "+nodeFromNumber
			offset = $("#"+id).offset()
			width = $("#"+id).width()
			height = $("#"+id).height()
			top = offset.top
			left = offset.left
			x1 = left+width-92;
			y1 = top+height/2;

			from = id.substring(4)  # because node has 4 characters
			$("#"+l2).children(".node").each () ->
				id = $(this).context.id
				foo = parseId(id)
				nodeToNumber = foo[1]
				# console.log "to: "+nodeToNumber
				offset = $("#"+id).offset()
				height = $("#"+id).height()
				top = offset.top
				left = offset.left
				x2 = left+7
				y2 = top+(height/2) # height/2 for straight line. height for down-corner looking
				to = id.substring(4) # because node has 4 characters

				createLine(x1,y1,x2,y2,false,from,to)
				if array[from] == undefined
					array[from] = []  # create array for the first time
				if array[from].indexOf(to) > -1
					writeMessage "you do have this nodes linked"
				else
					array[from].push(to)
					array[from].sort()
	else
		$("#"+l2).children(".node").each () ->
			id = $(this).context.id
			foo = parseId(id)
			nodeToNumber = foo[1]
			console.log nodeToNumber
			offset = $("#"+id).offset()
			height = $("#"+id).height()
			top = offset.top
			left = offset.left
			x2 = left+7
			y2 = top+(height/2) # height/2 for straight line. height for down-corner looking
			to = id.substring(4) # because node has 4 characters
			
			createLine(x1,y1,x2,y2,false,from,to)
			if array[from] == undefined
				array[from] = []  # create array for the first time
			if array[from].indexOf(to) > -1
				writeMessage "you do have this nodes linked"
			else
				array[from].push(to)
				array[from].sort()

	console.log array
	fixLines()

dragHoverLevel = (ev) ->
	ev.preventDefault()
	id = ev.target.parentNode.parentNode.id
	ev.target.style.background = "red"

dragLeaveLevel = (ev) ->
	ev.preventDefault()
	ev.target.style.background = "#aaa"

dragStart = (ev) ->
	id = ev.target.parentNode.parentNode.id
	linkButton = ev.target.className
	foo = parseId(id)
	nodeFromNumber = foo[1] # we need the last character(s) from string (example:) node1x2
	from = id.substring(4) # because node has 4 characters
	levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	width = $("#"+id).width()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x1 = left+width-92;
	y1 = top+height/2;


dragHover = (ev) ->
	ev.preventDefault()
	id = ev.target.parentNode.parentNode.id
	foo = parseId(id)
	nodeToNumber = foo[1] # we need the last character(s) from string (example:) node1x2
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x2 = left+7
	y2 = top+(height/2) # height/2 for straight line. height for down-corner looking
	if levelTo > levelFrom && thinLine == 0
		ev.target.style.background = "#CCCCFF" #blue
		createLine(x1,y1,x2,y2,true)
		thinLine++

	if levelTo <= levelFrom
		ev.target.style.background = "#FFAAAA"

dragLeave = (ev) ->
	defaultBackground(ev)
	$(".linetemp").remove()
	thinLine = 0


deleteLink = (ev) ->
	ev.preventDefault()
	defaultBackground(ev)
	to = ev.target.parentNode.parentNode.id.substring(4) # because node has 4 characters
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	if levelTo > levelFrom
		selector = "#"+from+"y"+to
		# console.log selector
		if $(selector).length > 0 # exists
			el = array[from].indexOf(to) #find link in array
			array[from].splice(el, 1) # delete link from array

			if array[from].length == 0 # if last link deleted
				delete array[from]

			$(selector).remove()
			$("#node"+to).children(".nodeCanvas").children(".nodeText").css({"border-color": "#777"})
		else
			writeError "this nodes are not linked"
	else
		writeError "deleting is allowed only to higher level node"
	$(".linetemp").remove()
	thinLine = 0


addLink = (ev) ->
	ev.preventDefault()
	defaultBackground(ev)
	to = ev.target.parentNode.parentNode.id.substring(4) # because node has 4 characters
	levelTo = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	if levelTo > levelFrom
		if array[from] == undefined
			array[from] = []  # create array for the first time

		if array[from].indexOf(to) > -1
			writeMessage "you do have this nodes linked"
		else
			array[from].push(to)
			array[from].sort()
			writeMessage "linked "+from+" to "+to
			createLine(x1,y1,x2,y2,false,from,to) #draw line between nodes

			$("#node"+to).children(".nodeCanvas").children(".nodeText").css({"border-color": "red"})
			# console.log array # debugging
		$(".linetemp").remove()
		thinLine = 0
	else
		writeError "can not link to current or previous level"

drop = (ev) ->
	console.log array
	if linkButton == "dndButton"
		addLink(ev)
	else if linkButton == "deleteDragButton"
		deleteLink(ev)
	else if linkButton == "linkButton"
		addLink(ev)
	else if linkButton == "deleteLinkButton"
		deleteLink(ev)
	fixLines()


clickStart = (ev) ->
	id = ev.target.parentNode.parentNode.id
	nodeFromNumber = id.substring(6) # we need the last character(s) from string (example:) node1x2
	from = id.substring(4) # because node has 4 characters
	levelFrom = ev.target.parentNode.parentNode.parentNode.id.substring(5) # because level has 5 characters
	offset = $("#"+id).offset()
	width = $("#"+id).width()
	height = $("#"+id).height()
	top = offset.top
	left = offset.left
	x1 = left+width-92;
	y1 = top+height/2;
	linkButton = ev.target.className
	clickButtonActive = true

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                         algorythm                         ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

merge = (j) ->
	# adding linked node to path
	coolArray[a].push(toArray[j])
	console.log "pushing "+toArray[j]+". now "+coolArray[a]+" in ["+a+"]"

	q = 0
	count = 0
	position = []
	check = toArray[j]

	# how many childrens has node
	while q < fromArray.length
		if check == fromArray[q]
			count++
			position.push(q)
		q++

	if count == 0 
		# the last node
		# console.log "this is the end, my only friend"
	else if count == 1 
		# the only one node
		merge(position[0])
	else
		# if more than one children
		k = 0
		while k < count
			# console.log "creating new chain"
			if k != count-1
				thisArray = coolArray[a].slice();
				coolArray[a+1] = coolArray[a].slice();
			else
				merge(position[k])
				wtf = true
				break

			merge(position[k])
			a++
			if wtf
				coolArray[a] = thisArray
				wtf = false
			k++


lala = (first,second) ->
	arrayR = []
	if first.length == 0 && second.length == 0
		# nothing :) return empty arrayR
	else if first.length == 0 && second.length > 0
		j = 0
		while j < second.length
			string = second[j]
			arrayR.push(string)
			j++
	else if first.length > 0 && second.length == 0
		i = 0
		while i < first.length
			string = first[i]
			arrayR.push(string)
			i++
	else
		i = 0
		while i < first.length
			j = 0
			while j < second.length
				string = first[i]+" "+second[j]
				arrayR.push(string)
				j++
			i++
	return arrayR




combine = (array) ->
	i = 0
	result = []
	while i < array.length
		temp = []
		j = 0
		while j < array[i].length-1
			if temp?.length == 0 # first node of path
				temp1 = $("#node"+array[i][j]).find(".nodeText").val().split(/\n/)
				arr1 = []
				k = 0
				while k < temp1.length
					arr1.push $.trim(temp1[k])  if /\S/.test(temp1[k])
					k++
			else 
				arr1 = temp
			

			temp2 = $("#node"+array[i][j+1]).find(".nodeText").val().split(/\n/)
			arr2 = []
			k = 0
			while k < temp2.length
				arr2.push $.trim(temp2[k])  if /\S/.test(temp2[k])
				k++
			

			temp = lala(arr1,arr2)
			# console.log temp

			j++
		i++
		result.push(temp)
	return result


start = ->
	size = Object.size(array)
	keys = Object.keys(array)
	
	firstNodes = []
	uniqueFirstNodes = []
	i = 0
	while i < size
		j = 0
		while j < array[keys[i]].length
			fromArray.push(keys[i])
			toArray.push(array[keys[i]][j])
			j++
		i++

	i = 0
	while i < fromArray.length
		unique = true
		j = 0
		while j < toArray.length
			if fromArray[i] == toArray[j]
				unique = false
				break
			j++
		if unique
			firstNodes.push(fromArray[i])
		i++

	$.each firstNodes, (i, el2) ->
		if $.inArray(el2, uniqueFirstNodes) is -1
			uniqueFirstNodes.push el2
	uniqueFirstNodes.sort()
	# console.log uniqueFirstNodes


	i = 0
	while i < uniqueFirstNodes.length
		j = 0
		while j < fromArray.length
			if uniqueFirstNodes[i] == fromArray[j]
				coolArray[a] = []
				coolArray[a].push(fromArray[j]) #add starting node
				console.log "beginning: pushing "+fromArray[j]+". now "+coolArray[a]+" in ["+a+"]"
				merge(j)
				a++
			j++
		i++
	console.log coolArray
	outputArrow = combine(coolArray)

	i=0
	if $("#checkboxSortOutput").is ":checked"
		while i < outputArrow.length
			outputArrow[i].sort()
			i++


	$("body").append("<div class='outputContainer'></div>")

	i=0
	while i < outputArrow.length
		$(".outputContainer").append $(outputNodeCode).attr("id","output"+i)
		$("#output"+i).children("textarea").val(outputArrow[i].join("\n"))

		j = 0
		outputNameString = ''
		while j < coolArray[i].length
			if $("#node"+coolArray[i][j]).children(".buttons").children(".nodeName").val() == "" # if node has no name
				outputNameString += coolArray[i][j]
			else 
				outputNameString += $("#node"+coolArray[i][j]).children(".buttons").children(".nodeName").val()
			if j != coolArray[i].length-1
				outputNameString +=" "
			j++
		$("#output"+i).children(".outputNodeName").html(outputNameString)
		i++

	$(".coffeeModal").modal('hide')

	if $("#checkboxPerformance").is ":checked"
		$(".levelContanter").remove()
	else
		$.scrollTo(".outputContainer")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################
#                     document handlers                     ###################################################################################################
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ###################################################################################################

$(document).on "click", "#doneButton", ->
	doneButtonClick()

$(document).on "click", "#backButton", ->
	backButtonClick()

$(document).on "click", "#startButton", ->
	if $("#checkboxPerformance").is ":checked"
		$(".levelButtonsWrapper").remove()
		$(".addNodeButtonWrapper").remove()
		$(".linkButton").remove()
		$(".dndButton").remove()
		$(".deleteLinkButton").remove()
		$(".deleteDragButton").remove()
		$(".editButton").remove()
		$(".delButton").remove()
		$(".line").remove()
		$(".nodeName").hide()
		$(".matrixPosition").hide()

	$(".coffeeModal").modal()
	setTimeout (->
		start()
	), 500
	
	$('#outputSlider').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0})
	$("#startButton").fadeOut()
	$("#csvExportButton").fadeIn()


$(document).on "click", "#csvExportButton", ->
	csvString = ""
	if $("#checkboxPerformance").is ":checked"
		$(".levelContanter").remove()
		$(".lastLevelModal").remove()
		$(".editModal").remove()
		$(".deleteNodeModal").remove()
		$(".deleteLevelModal").remove()
		$(".coffeeModal").remove()
		$(".controlPanel").remove()
		$("#outputSlider").remove()

	$(".outputNodeName").each (i) ->
		csvString+=$(this).text()
		csvString+=","
		csvString+=$(this).next(".outputNodeContent").val().replace(/\n/g, ",")
		if i != $(".outputNodeName").length-1
			csvString+="\n"
		if $("#checkboxPerformance").is ":checked"
			$(".outputNodeName").parent().get(i).remove()


	# console.log "L: "+csvString.length
	if csvString.length > 1550396
		writeError("sorry, can not generate .csv file, too big size, work manual")
	else
		a = document.createElement("a")
		a.href = "data:attachment/csv," + encodeURIComponent(csvString)
		a.target = "_blank"
		a.download = "comgen2 "+getTime(false)+".csv"
		document.body.appendChild a
		a.click()

$(document).on "click", ".addNodeButton", ->
	addNodeButtonClick($(this))

$(document).on "click", ".addFirstNode", ->
	addFirstNodeButtonClick($(this))

$(document).on "click", ".delButton", ->
	delButtonClick($(this))

$(document).on "click", "#deleteNodeYes", ->
	id = $(".deleteNodeModal").children(".hiddenInfo").val()
	deleteNode(id,true)

$(document).on "click", "#deleteLevelYes", ->
	id = $(".deleteLevelModal").children(".hiddenInfo").val()
	deleteLevel(id,true)

$(document).on "click", ".removeLevelButton", ->
	removeLevelButtonClick($(this))


$(document).on "click", "#editModalDoneButton", ->
	parent = $(this).parent()
	newContent = $(parent).children("#editModalTextbox").val()

	if $("#allLO").is(':checked')
		newContent = newContent.toLowerCase()
	if $("#allUP").is(':checked')
		newContent = newContent.toUpperCase()
	if $("#wordsUP").is(':checked')
		newContent = uppercaseFirstLetterAllWords(newContent)
	if $("#wordsLO").is(':checked')
		newContent = lowercaseFirstLetterAllWords(newContent)
	if $("#stringUP").is(':checked')
		newContent = newContent.toLowerCase()
		newContentArray = newContent.split('\n')
		newContent = ''
		$.each newContentArray, (i) ->
			newContentArray[i] = uppercaseFirstLetter(newContentArray[i])
			newContent+=newContentArray[i]
			if i != newContentArray.length-1
				newContent+='\n'
	if $("#stringLO").is(':checked')
		newContent = newContent.toUpperCase()
		newContentArray = newContent.split('\n')
		newContent = ''
		$.each newContentArray, (i) ->
			newContentArray[i] = lowercaseFirstLetter(newContentArray[i])
			newContent+=newContentArray[i]
			if i != newContentArray.length-1
				newContent+='\n'

	$("#"+root.editingNode).children(".nodeCanvas").children("textarea").val(newContent)
	$('.editModal').modal('hide');
	$("input[name=opt]:radio").removeAttr("checked");

$(document).on "keydown", "#editModalTextbox", (e) ->
	if (e.metaKey and e.keyCode is 13) or (e.ctrlKey and e.keyCode is 13)
		$("#editModalDoneButton").click()

$(document).on "click", ".editButton", ->
	editButtonClick($(this))
	
$(document).on "click", "#settingsButton", ->
	$(".settingsModal").modal()

$(document).on "click", ".addLevelButton", ->
	addFirstNode(createLevel())
	deleteNodeButtons()
	addNodeButtons()
	deleteLevelButton()
	addLevelButton()
	writeId()
	tabIndexOrder()

$(document).ready ->
	addFirstNode(createLevel())
	addFirstNode(createLevel())
	writeId()
	tabIndexOrder()
	writeMessage("welcome to comgen2 beta v0.5.141203fac")
	
$(window).load ->
	addNodeButtons()
	addLevelButton()

	widthSlider = new slider("outputSlider", 400, 10, 99, 0)

$(document).on "mouseenter", ".nodeCanvas", (event) ->
	if clickButtonActive
		dragHover(event)
	else
		$(".line").css({visibility: "hidden"})
		id = event.target.parentNode.parentNode.id
		foo = id.substring(4) # because node has 4 characters
		bar = 'div.line[id^="'+foo+'y"]'
		$(bar).css({visibility: "visible"})


$(document).on "mouseleave", ".nodeCanvas", (event) ->
	if clickButtonActive
		dragLeave(event)
	else
		$(".line").css({visibility: "visible"})

$(document).keyup (e) ->
	if e.keyCode is 27 # esc
		clickButtonActive = false
		$(".linetemp").remove()
		$(".nodeText").css({background: "#EBEBE4"})


$(document).on "click", ".nodeCanvas", (ev) ->
	if clickButtonActive
		clickButtonActive = false
		drop(ev)